
setup
```
name="webapp-nginx-menu"; script="setup-$name.sh"; curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script && bash $script && rm $script

```

https://gitlab.com/gllmar/nginx-menu



Install NGINX:

sudo apt install nginx -y

sudo systemctl start nginx

sudo systemctl enable nginx






Check NGINX Installation:

    You can check if NGINX is running by entering your Raspberry Pi’s IP address in a web browser. You should see a welcome page.



sudo nginx -t

    If the configuration is okay, reload NGINX to apply the changes.

bash

sudo systemctl reload nginx

Place Your Content:

    Place your content in the directory you specified in the NGINX configuration (/var/www/html/your-directory). For example, you could place an index.html file here.