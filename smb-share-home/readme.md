
# Samba Home Share Setup Script

## easy-install
```
name="smb-share-home"; script="setup-$name.sh"; curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script &&  bash $script && rm $script
```

This script is designed to easily set up a Samba share for a specified user's home directory on a Debian/Ubuntu-based system.
Requirements
Samba Home Share Setup

## Features

* Checks if Samba is installed and installs it if necessary.
* Backs up any existing Samba configuration.
* Prompts for user input or uses supplied parameters for configuration.
* Sets up the Samba user and password.
* Restarts and enables necessary services.
* Configures the firewall (if UFW is available).

