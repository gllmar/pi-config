#!/bin/bash

LOG_FILE="samba_setup.log"
echo "Logging to $LOG_FILE..."

get_input() {
    local prompt="$1"
    local default_value="$2"
    local input
    read -p "$prompt [$default_value]: " input
    input="${input:-$default_value}"
    echo "You selected: $input" >&2
    echo "$input"
}

# Check if Samba is installed
if ! which samba > /dev/null; then
    echo "Installing Samba..."
    sudo apt update && sudo apt install -y samba samba-common-bin | tee -a $LOG_FILE
else
    echo "Samba already installed."
fi

# Check if Samba is already configured
if [ -f /etc/samba/smb.conf ]; then
    echo "Existing Samba configuration detected."
    redo_config=$(get_input "Do you want to redo the Samba configuration? (yes/no)" "no")
    if [ "$redo_config" != "yes" ]; then
        echo "Skipping Samba configuration as per user request."
        exit 0
    fi
fi

# Backup existing configuration if needed
if [ -f /etc/samba/smb.conf ] && [ ! -f /etc/samba/smb.conf.backup ]; then
    echo "Backing up existing smb.conf..."
    sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.backup
elif [ -f /etc/samba/smb.conf.backup ]; then
    echo "Backup already exists. Skipping backup..."
fi

# Gather user inputs
username=$(get_input "Enter the username for which you want to set up the Samba share" "$USER")
workgroup=$(get_input "Enter your workgroup name" "WORKGROUP")
share_path=$(get_input "Enter path to be shared (e.g. /home/$username)" "/home/$username")
readonly_opt=$(get_input "Should the share be read-only? (yes/no)" "no")
guest_ok_opt=$(get_input "Should guest access be allowed? (yes/no)" "no")

# Write new smb.conf
cat <<EOL | sudo tee /etc/samba/smb.conf
[global]
  workgroup = $workgroup
  client min protocol = NT1
  server min protocol = NT1
  security = user
  map to guest = Bad User
  wins support = yes

[home]
  path = $share_path
  browsable = yes
  read only = $readonly_opt
  guest ok = $guest_ok_opt
EOL

# Setup Samba user
echo "Setting up Samba user..."
echo "Please enter a password for Samba user ($username):"
sudo smbpasswd -a $username

# Restart and enable services
echo "Starting, restarting, and enabling services..."
sudo systemctl restart smbd || { echo "Error restarting smbd. Exiting."; exit 1; }
sudo systemctl enable smbd
sudo systemctl restart nmbd || { echo "Error restarting nmbd. Exiting."; exit 1; }
sudo systemctl enable nmbd

# Configure firewall if UFW is available
if which ufw > /dev/null; then
    echo "Configuring firewall..."
    sudo ufw allow samba
else
    echo "UFW not found or not active. Ensure your firewall allows Samba."
fi

echo "Samba configuration completed successfully!"

# Offer to clear the log file
clear_log=$(get_input "Do you want to clear the log file? (yes/no)" "yes")
if [ "$clear_log" = "yes" ]; then
    echo "Clearing the log file..."
    >$LOG_FILE
    rm $LOG_FILE
    echo "Log file cleared!"
else
    echo "Log file retained."
fi
