#!/bin/bash

# Associative array for version to link mapping
declare -A VERSION_LINKS

VERSION_LINKS["21.0.0.0_beta"]="https://github.com/awawa-dev/HyperHDR/releases/download/v21.0.0.0beta1/HyperHDR-21.0.0.0.bookworm"
VERSION_LINKS["20.0.0.0"]="https://github.com/awawa-dev/HyperHDR/releases/download/v20.0.0.0/HyperHDR-20.0.0.0.bookworm"
VERSION_LINKS["19.0.0.0"]="https://github.com/awawa-dev/HyperHDR/releases/download/v19.0.0.0/HyperHDR-19.0.0.0"

# Function to display version selection menu
display_version_menu() {
    echo "Available versions:"
    local i=1
    for version in "${!VERSION_LINKS[@]}"; do
        echo "$i) $version"
        ((i++))
    done
}

# Function to generate service files
generate_service_files() {
    # Define the path where the service files will be saved
    SYSTEMD_USER_DIR="$HOME/.config/systemd/user"
    mkdir -p $SYSTEMD_USER_DIR

    # Try to detect Wayfire (a Wayland compositor) using pgrep
    if pgrep -x "wayfire" > /dev/null; then
        WINDOW_MANAGER="wayland"
    else
        # If Wayfire is not running, detect the window manager (Wayland or X11) using loginctl
        WINDOW_MANAGER=$(loginctl show-session $(loginctl list-sessions | awk '/[0-9]/{print $1}') -p Type | cut -d= -f2)
    fi

    if [[ $WINDOW_MANAGER == "wayland" ]]; then
        # Generate wait-for-wf-panel-pi.service for Wayland
        cat <<EOL > $SYSTEMD_USER_DIR/wait-for-wf-panel-pi.service
[Unit]
Description=Wait for wf-panel-pi to start

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/bin/bash -c 'while ! pgrep -x "wf-panel-pi" > /dev/null; do sleep 1; done; sleep 20'

[Install]
WantedBy=default.target
EOL

        # Generate hyperhdr.service for Wayland
        cat <<EOL > $SYSTEMD_USER_DIR/hyperhdr.service
[Unit]
Description=HyperHDR User Service
After=wait-for-wf-panel-pi.service
Requires=wait-for-wf-panel-pi.service

[Service]
Type=simple
ExecStart=/usr/bin/hyperhdr
Restart=always
RestartSec=5
Environment=DISPLAY=:0

[Install]
WantedBy=default.target
EOL
    else
        # Generate hyperhdr.service for X11
        cat <<EOL > $SYSTEMD_USER_DIR/hyperhdr.service
[Unit]
Description=HyperHDR User Service

[Service]
Type=simple
ExecStart=/usr/bin/env DISPLAY=:0 /usr/bin/hyperhdr
Restart=always
RestartSec=5

[Install]
WantedBy=default.target
EOL
    fi
}

# Main installation and setup script starts here
echo "Welcome to HyperHDR Installer"

# Detect architecture
ARCH=$(uname -m)
DEB_EXTENSION=""

case $ARCH in
    armv6l)
        DEB_EXTENSION="-armv6l.deb"
        ;;
    aarch64)
        DEB_EXTENSION="-aarch64.deb"
        ;;
    x86_64)
        DEB_EXTENSION="-x86_64.deb"
        ;;
    *)
        echo "Unsupported architecture: $ARCH"
        exit 1
        ;;
esac

# Display version selection menu
display_version_menu

# Default to the latest version
echo "Select a version to install (default is the latest): "
read -r version_choice
version_keys=("${!VERSION_LINKS[@]}")
selected_version=${version_keys[$version_choice-1]}

# Fallback to the latest version if no valid input is given
if [ -z "$selected_version" ]; then
    selected_version=${version_keys[0]}
fi

echo "Selected version: $selected_version"

# Construct download link
DOWNLOAD_LINK="${VERSION_LINKS[$selected_version]}$DEB_EXTENSION"
echo "Download link: $DOWNLOAD_LINK"

# Check if hyperhdr exists in /usr/bin/hyperhdr
if [[ ! -f /usr/bin/hyperhdr ]]; then
    echo "hyperhdr not found. Installing..."

    # Download the appropriate .deb package
    wget "$DOWNLOAD_LINK"

    # Install the .deb package
    sudo apt install ./${DOWNLOAD_LINK##*/}

    # Prompt user to remove the .deb package
    read -p "Do you want to remove the downloaded .deb package? (y/n) " remove_choice
    if [[ $remove_choice == "y" || $remove_choice == "Y" ]]; then
        rm ${DOWNLOAD_LINK##*/}
        echo "Removed ${DOWNLOAD_LINK##*/}."
    fi
else
    echo "hyperhdr is already installed."
fi

# Prompt user to generate systemd service files
read -p "Do you wish to create systemd user service files? (y/n) " service_user_choice
if [[ $service_user_choice == "y" || $service_user_choice == "Y" ]]; then
    generate_service_files
    echo "Systemd user service files have been created."
fi

# Ask the user if they want to disable the root service and enable the user service
read -p "Do you want to disable the root service (hyperhdr@pi.service) and activate the user service? (y/n) " service_choice
if [[ $service_choice == "y" || $service_choice == "Y" ]]; then
    sudo systemctl stop hyperhdr@pi.service
    sudo systemctl disable hyperhdr@pi.service
    echo "Root service hyperhdr@pi.service has been stopped and disabled."

    # Reload the systemd user manager to recognize the new service
    systemctl --user daemon-reload
    systemctl --user enable hyperhdr.service
    echo "HyperHDR user service file has been installed."
fi

# Prompt the user to restart the user service
read -p "Do you want to restart the HyperHDR user service now? (y/n) " restart_choice
if [[ $restart_choice == "y" || $restart_choice == "Y" ]]; then
    systemctl --user restart hyperhdr.service
    echo "HyperHDR user service has been restarted."
fi

# Ask the user if they want to add a redirection in nginx configuration
read -p "Do you want to add a redirection in nginx conf.d? (y/n) " nginx_choice
if [[ $nginx_choice == "y" || $nginx_choice == "Y" ]]; then
    # Create a temporary file with the specified content
    echo -e "location ~ ^/hyperhdr(/.*)?$ {\n\t# Use return for a straightforward redirect\n\treturn 302 http://\$host:8090\$1;\n}" > /tmp/hyperhdr_forward
    
    # Move the temporary file to the nginx conf.d directory with sudo privileges
    sudo mv /tmp/hyperhdr_forward /etc/nginx/conf.d/hyperhdr_forward
    echo "Redirection added to nginx conf.d as hyperhdr_forward."

    # Reload nginx
    echo "Check nginx config"
    sudo nginx -t
    if [ $? -eq 0 ]; then
        echo "Reloading nginx"
        sudo systemctl restart nginx
    else
        echo "Error in nginx configuration. Please check the syntax."
    fi
fi
