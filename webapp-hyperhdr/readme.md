install 
```
name="webapp-hyperhdr"; script="setup-$name.sh"; curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script && bash $script && rm $script

```

<!-- curl -sSL https://gitlab.com/gllmar/pi-config/-/raw/main/webapp-hyperhdr/setup.sh -o setup-webapp-hyperhdr.sh && bash webapp-hyperhdr.sh && rm webapp-hyperhdr.sh -->


https://github.com/awawa-dev/HyperHDR/releases/download/v19.0.0.0/HyperHDR-19.0.0.0-Linux-aarch64.deb



for user service -> 
bash 
```
<(curl -sL https://gitlab.com/gllmar/sysd-hyperhdr/-/raw/main/install_hyperhdr_user_service.sh)
```



    location ~ ^/hyperhdr(/.*)?$ {                                                                                         
        # Use return for a straightforward redirect                                                                        
        return 302 http://$host:8090$1;                                                                                    
    }      