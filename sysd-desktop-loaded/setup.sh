#!/bin/bash

# Define the service file content
service_content="[Unit]
Description=Wait for wf-panel to finish loading
After=graphical-session.target

[Service]
Type=oneshot
ExecStart=/bin/true
ExecStartPost=/bin/bash -c \"while ! pgrep -x 'wf-panel-pi' > /dev/null; do sleep 1; done\"

[Install]
WantedBy=default.target
"

# Ask the user if they want to install the service
read -p "Do you want to install the wait-for-desktop-loaded user service? (y/n) " answer
if [[ "$answer" == "y" ]]; then
    # Create the service file in the user's systemd directory
    mkdir -p ~/.config/systemd/user
    echo "$service_content" > ~/.config/systemd/user/wait-for-desktop-loaded.service

    # Reload systemd manager configuration
    systemctl --user daemon-reload

    # Ask if the user wants to start and enable the service
    read -p "Do you want to start and enable the service at boot? (y/n) " start_enable
    if [[ "$start_enable" == "y" ]]; then
        systemctl --user enable --now wait-for-desktop-loaded.service
        echo "User service started and enabled at boot."
    else
        echo "User service installed but not started or enabled."
    fi
else
    echo "Service installation canceled."
fi
