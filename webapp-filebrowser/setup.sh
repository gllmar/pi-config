#!/bin/bash

# Prompt user for port selection with default to 8060
read -p "Enter the port you want FileBrowser to listen on [8060]: " filebrowser_port
filebrowser_port=${filebrowser_port:-8060}

read -p "Enter the password you want for user $USER [password]: " filebrowser_password
filebrowser_password=${filebrowser_password:-password}

# Function to install FileBrowser
install_filebrowser() {
    echo "Installing FileBrowser..."
    curl -fsSL https://raw.githubusercontent.com/filebrowser/get/master/get.sh | bash
}

# Function to initialize FileBrowser configuration with the selected port
init_filebrowser() {
    echo "Initializing FileBrowser configuration on port $filebrowser_port..."
    filebrowser config init -d /home/pi/.config/filebrowser/filebrowser.db -p $filebrowser_port -a 0.0.0.0 
    filebrowser users add  -d /home/pi/.config/filebrowser/filebrowser.db $USER $filebrowser_password     
}

# Function to create FileBrowser systemd user service
create_user_service() {
    mkdir -p ~/.config/systemd/user

    # Writing the FileBrowser service file content with the selected port
    cat > ~/.config/systemd/user/filebrowser.service <<EOF
[Unit]
Description=Filebrowser
After=network-online.target

[Service]
ExecStart=/usr/local/bin/filebrowser -d /home/pi/.config/filebrowser/filebrowser.db -r /home/pi/ -p $filebrowser_port

[Install]
WantedBy=default.target
EOF

    # Reload systemd daemon and enable FileBrowser service
    systemctl --user daemon-reload
    systemctl --user enable filebrowser.service
    systemctl --user start filebrowser.service
    echo "FileBrowser user service created and started on port $filebrowser_port."
}

# Function to setup Nginx forwarding with the selected port
setup_nginx_forwarding() {
    echo "Setting up Nginx location for FileBrowser on port $filebrowser_port..."
    local nginx_conf="/etc/nginx/conf.d/filebrowser_forwarding"
    echo "
    location ~ ^/filebrowser(/.*)?$ {
        # Use return for a straightforward redirect
        return 302 http://\$host:$filebrowser_port\$1;
    }
    location ~ ^/file(/.*)?$ {
        # Use return for a straightforward redirect
        return 302 http://\$host:$filebrowser_port\$1;
    }
    location ~ ^/files(/.*)?$ {
        # Use return for a straightforward redirect
        return 302 http://\$host:$filebrowser_port\$1;
    }
    " | sudo tee $nginx_conf
    sudo systemctl restart nginx
}

# Check if filebrowser is installed
if command -v filebrowser &> /dev/null; then
    echo "FileBrowser is already installed."
else
    install_filebrowser
    init_filebrowser
fi

# Check if a user service for filebrowser exists
if systemctl --user --type=service --state=active | grep -q filebrowser; then
    echo "A user service for FileBrowser already exists."
else
    create_user_service
fi

# Optional: Setup Nginx forwarding
read -p "Do you want to set up Nginx forwarding for FileBrowser? [Y/n] " setup_nginx
setup_nginx=${setup_nginx:-yes}

if [[ $setup_nginx =~ ^[Yy](es)?$ ]]; then
    if command -v nginx &> /dev/null; then
        setup_nginx_forwarding
    else
        echo "Nginx is not installed. Cannot set up forwarding."
    fi
fi

echo "FileBrowser setup complete."
