#!/bin/bash

# Function to install Godot
install_godot() {
    local version="$1"
    local mono_support="$2"
    local install_dir="$3"

    # URLs
    RELEASES_URL="https://api.github.com/repos/godotengine/godot/releases/tags/$version"
    DESKTOP_FILE_URL="https://raw.githubusercontent.com/godotengine/godot/master/misc/dist/linux/org.godotengine.Godot.desktop"
    ICON_URL="https://raw.githubusercontent.com/godotengine/godot/master/icon.png"

    # Fetch the release data
    release_data=$(curl -s "$RELEASES_URL")

    # Determine file extension based on mono support
    local file_extension
    if [ "$mono_support" = true ]; then
        file_extension="mono_.*\\.zip$"
    else
        file_extension="\\.(zip|tar\\.gz|tar\\.xz)$"
    fi

    # Prepare a regex pattern for jq
    pattern=".*_${OS}.${ARCH}${file_extension}"

    # Extract available download options
    mapfile -t download_options < <(echo "$release_data" | jq -r --arg pattern "$pattern" '.assets[] | select(.name | test($pattern)) | .name')

    # Check if any options are available
    if [ ${#download_options[@]} -eq 0 ]; then
        echo "No compatible packages found."
        return 1
    fi

    # Prompt for user choice
    echo "Available downloads for $version:"
    select option in "${download_options[@]}"; do
        if [[ " ${download_options[*]} " =~ " ${option} " ]]; then
            download_url=$(echo "$release_data" | jq -r --arg name "$option" '.assets[] | select(.name == $name) | .browser_download_url')
            break
        else
            echo "Invalid selection. Please try again."
        fi
    done

    # Download and extract
    local godot_dir="$install_dir/godot-${version}"
    mkdir -p "$godot_dir"
    wget -q "$download_url" -O "$godot_dir/$option"
    if [[ "$option" == *.zip ]]; then
        unzip -d "$godot_dir" "$godot_dir/$option"
    elif [[ "$option" == *.tar.gz ]] || [[ "$option" == *.tar.xz ]]; then
        tar -xf "$godot_dir/$option" -C "$godot_dir"
    fi

    # Create desktop integration
    local desktop_file="$godot_dir/org.godotengine.Godot.desktop"
    local modified_desktop_file="$godot_dir/org.godotengine.Godot-$version.desktop"
    local executable_name=$(basename "$(find "$godot_dir" -type f -executable -name 'Godot*' | head -n 1)")

    curl -s -o "$desktop_file" "$DESKTOP_FILE_URL"
    sed -i "s|Exec=godot %f|Exec=$godot_dir/$executable_name %f|" "$desktop_file"
    sed -i "s|^Name=Godot Engine|Name=Godot Engine $version|" "$desktop_file"
    mv "$desktop_file" "$modified_desktop_file"
    sudo desktop-file-install "$modified_desktop_file"

    # Set up the icon
    curl -s -o "$godot_dir/godot-icon.png" "$ICON_URL"
    sudo cp "$godot_dir/godot-icon.png" "/usr/share/pixmaps/godot-$version.png"



}

# Main installation function
main() {
    # Detect OS and architecture
    OS=$(uname -s | tr '[:upper:]' '[:lower:]')
    ARCH=$(uname -m)
    case "$ARCH" in
        x86_64 | amd64) ARCH="x86_64" ;;
        arm*) ARCH="arm" ;;
        aarch64) ARCH="arm64" ;;
        i386 | i486 | i586 | i686) ARCH="x86_32" ;;
        *) echo "Unknown architecture: $ARCH"; exit 1 ;;
    esac

    echo "Detected OS: $OS, Architecture: $ARCH"

    # Check for jq installation
    if ! command -v jq &> /dev/null; then
        echo "jq could not be found, installing..."
        sudo apt-get update
        sudo apt-get install -y jq
    fi

    # Fetch available Godot versions
    RELEASES_URL="https://api.github.com/repos/godotengine/godot/releases"
    versions_data=$(curl -s "$RELEASES_URL")
    mapfile -t godot_versions < <(echo "$versions_data" | jq -r '.[].tag_name')

    # Menu to select version
    echo "Select a Godot version to install:"
    select version in "${godot_versions[@]}"; do
        if [[ " ${godot_versions[*]} " =~ " ${version} " ]]; then
            echo "Do you need Mono support? [y/N]"
            read -r mono_reply
            local mono_support=false
            if [[ $mono_reply =~ ^[Yy]$ ]]; then
                mono_support=true
            fi

            echo "Enter the installation directory or leave blank for default ($HOME/.local/bin):"
            read -r install_dir
            install_dir=${install_dir:-"$HOME/.local/bin"}

            install_godot "$version" "$mono_support" "$install_dir"
            break
        else
            echo "Invalid selection. Please try again."
        fi
    done
}

# Run the main function
main
