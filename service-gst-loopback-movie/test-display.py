import gi
import threading
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

# Initialize GStreamer
Gst.init(None)

def create_pipeline(display_name):
    pipeline_desc = (
        f"v4l2src device=/dev/video61 ! "
        "videoconvert ! "
        "videoscale ! "
        f"waylandsink fullscreen=true display={display_name}"
    )
    pipeline = Gst.parse_launch(pipeline_desc)
    pipeline.set_state(Gst.State.PLAYING)
    return pipeline

def run_pipeline(display_name):
    pipeline = create_pipeline(display_name)
    GLib.MainLoop().run()

# Launch pipelines for each display in separate threads
displays = ['wayland-0', 'wayland-1']
threads = []

for display in displays:
    thread = threading.Thread(target=run_pipeline, args=(display,))
    threads.append(thread)
    thread.start()

# Wait for all threads to finish
for thread in threads:
    thread.join()
