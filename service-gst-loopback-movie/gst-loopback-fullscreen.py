import gi
import sys
import argparse

gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

def on_message(bus, message, loop, player):
    if message.type == Gst.MessageType.EOS:
        loop.quit()
    elif message.type == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(f"Error: {err}, {debug}", file=sys.stderr)
        loop.quit()
    return True

def select_sink(monitor_index=None):
    registry = Gst.Registry.get()
    sinks = []
    for feature in registry.get_feature_list(Gst.ElementFactory):
        if 'Sink/Video' in feature.get_metadata("klass"):
            sinks.append(feature.get_name())

    sink_choice = 'waylandsink' if 'waylandsink' in sinks else 'autovideosink'
    print(f"Using {sink_choice} for video output.")
    return sink_choice, monitor_index

def main():
    Gst.init(None)

    parser = argparse.ArgumentParser(description='Display loopback video source in fullscreen on Wayland.')
    parser.add_argument('--loopback-device', default='/dev/video61', help='Loopback device path. Default is /dev/video61.')
    parser.add_argument('--monitor', type=int, help='Monitor index for fullscreen display. If not provided, defaults to the first monitor.')
    args = parser.parse_args()

    video_sink, monitor_index = select_sink(args.monitor)
    pipeline_desc = f'v4l2src device={args.loopback_device} ! videoconvert ! videoscale'

    if video_sink == 'waylandsink':
        if monitor_index is not None:
            pipeline_desc += f' ! waylandsink fullscreen=true monitor={monitor_index}'
        else:
            pipeline_desc += ' ! waylandsink fullscreen=true'
    else:
        pipeline_desc += ' ! ' + video_sink

    player = Gst.parse_launch(pipeline_desc)
    bus = player.get_bus()
    bus.add_watch(0, on_message, GLib.MainLoop(), player)

    player.set_state(Gst.State.PLAYING)

    try:
        GLib.MainLoop().run()
    except KeyboardInterrupt:
        player.set_state(Gst.State.NULL)
        print("\nFullscreen display interrupted.")

if __name__ == '__main__':
    main()