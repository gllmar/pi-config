import os
import gi
import subprocess
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

def list_drm_devices():
    drm_dir = '/dev/dri/'
    devices = []

    try:
        for device in os.listdir(drm_dir):
            if 'card' in device:
                devices.append(os.path.join(drm_dir, device))
    except Exception as e:
        print(f"Error listing DRM devices: {e}")

    return devices

def create_pipeline(device_path):
    # Set the environment variable for kmssink
    os.environ['GST_KMS_SINK_DEVICE'] = device_path

    pipeline_desc = (
        "v4l2src device=/dev/video61 ! "
        "videoconvert ! "
        "kmssink sync=false"
    )
    pipeline = Gst.parse_launch(pipeline_desc)
    pipeline.set_state(Gst.State.PLAYING)
    return pipeline

def main():
    Gst.init(None)
    pipelines = []

    drm_devices = list_drm_devices()
    if not drm_devices:
        print("No DRM devices found.")
        return

    for device in drm_devices:
        print(f"Setting up pipeline for {device}")
        pipeline = create_pipeline(device)
        pipelines.append(pipeline)

    # Start the main loop
    mainloop = GLib.MainLoop()
    try:
        mainloop.run()
    except KeyboardInterrupt:
        for pipeline in pipelines:
            pipeline.set_state(Gst.State.NULL)
        print("\nStopped playback")

if __name__ == "__main__":
    main()
