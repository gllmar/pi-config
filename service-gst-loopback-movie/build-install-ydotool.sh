#!/bin/bash

# Create a directory for dependencies
mkdir -p dependencies
cd dependencies

# Install required packages
sudo apt install scdoc cmake make g++ libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libevdev-dev libudev-dev libfmt-dev

# Clone the ydotool repository
git clone https://github.com/ReimuNotMoe/ydotool.git

# Navigate to the ydotool directory
cd ydotool

# Create a build directory and navigate into it
mkdir build
cd build

# Generate the makefile and build the project
cmake ..
make -j $(nproc)

# Install the built tools
sudo make install

# Copy the service file to the systemd directory
sudo cp ydotool.service /etc/systemd/system/

# Reload systemd to recognize the new service file
sudo systemctl daemon-reload

# Ask the user if they want to enable and start the ydotool service
read -p "Do you want to enable and start the ydotool service? (y/n) " answer
if [[ "$answer" == "y" ]]; then
    # Enable and start the service
    sudo systemctl enable ydotool
    sudo systemctl start ydotool
    echo "ydotool service enabled and started."
else
    echo "ydotool service not enabled."
fi

# Return to the original directory
cd ../../..
