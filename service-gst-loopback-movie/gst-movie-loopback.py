import gi
import sys
import argparse
import subprocess

gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

def on_message(bus, message, loop, player):
    if message.type == Gst.MessageType.EOS:
        player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, 0)
    elif message.type == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(f"Error: {err}, {debug}", file=sys.stderr)
        loop.quit()
    return True

def build_decoder_pipeline(codec, pi_model):
    if codec == 'h264':
        if 'Raspberry Pi 4' in pi_model:
            return 'h264parse ! v4l2h264dec'
        else:
            return 'h264parse ! avdec_h264'
    elif codec == 'hevc' or codec == 'h265':
        return 'h265parse ! avdec_h265'
    elif codec == 'vp8':
        return 'vp8dec'
    elif codec == 'vp9':
        return 'vp9dec'
    elif codec == 'av1':
        return 'av1dec'
    else:
        return 'decodebin'


def get_video_properties_ffprobe(filepath):
    command = [
        'ffprobe',
        '-v', 'error',
        '-select_streams', 'v:0',
        '-show_entries', 'stream=codec_name,r_frame_rate',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        filepath
    ]
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    if result.returncode == 0:
        codec, frame_rate = result.stdout.strip().split('\n')
        return codec, frame_rate
    else:
        print(f"Failed to get video properties: {result.stderr}", file=sys.stderr)
        sys.exit(1)

def get_raspberry_pi_model():
    try:
        with open('/proc/device-tree/model', 'r') as model_file:
            model = model_file.read()
        return model
    except Exception as e:
        print(f"Error reading Raspberry Pi model: {e}", file=sys.stderr)
        return None   

def main():
    Gst.init(None)

    parser = argparse.ArgumentParser(description='Play a video file to a loopback device.')
    parser.add_argument('filepath', help='The path to the video file to play.')
    parser.add_argument('--loopback-device', default='/dev/video61', help='Loopback device path. Default is /dev/video61.')
    args = parser.parse_args()
    pi_model = get_raspberry_pi_model()
    if pi_model is None:
        print("Unable to determine Raspberry Pi model. Exiting.")
        sys.exit(1)

    codec, frame_rate = get_video_properties_ffprobe(args.filepath)
    print(f"Detected codec: {codec}")
    print(f"Raspberry Pi Model: {pi_model}")


    decoder_pipeline = build_decoder_pipeline(codec, pi_model)
    pipeline_desc = f'filesrc location="{args.filepath}" ! qtdemux ! {decoder_pipeline} ! videoconvert ! videoscale'
    pipeline_desc += f' ! capsfilter caps="video/x-raw,framerate={frame_rate}"'
    pipeline_desc += f' ! v4l2sink device={args.loopback_device}'

    player = Gst.parse_launch(pipeline_desc)
    bus = player.get_bus()
    bus.add_watch(0, on_message, GLib.MainLoop(), player)

    player.set_state(Gst.State.PLAYING)

    try:
        GLib.MainLoop().run()
    except KeyboardInterrupt:
        player.set_state(Gst.State.NULL)
        print("\nPlayback to loopback device interrupted.")

if __name__ == '__main__':
    main()
