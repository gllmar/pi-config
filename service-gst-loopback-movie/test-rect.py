import os
import subprocess
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

def is_wayland():
    return 'WAYLAND_DISPLAY' in os.environ

def is_xorg():
    return 'DISPLAY' in os.environ

def parse_wlr_randr_output(output):
    displays = []
    for line in output.splitlines():
        if "Enabled: yes" in line:
            display_info = line.split()
            display_name = display_info[0]
            resolution = display_info[2].split('+')[0].split('x')  # Format: WIDTHxHEIGHT+X+Y
            width, height = map(int, resolution)
            displays.append((display_name, width, height))
    return displays

def get_wayland_displays():
    try:
        output = subprocess.check_output(['wlr-randr']).decode()
        return parse_wlr_randr_output(output)
    except Exception as e:
        print(f"Error getting Wayland displays: {e}")
        return []

def get_xorg_displays():
    try:
        output = subprocess.check_output(['xrandr']).decode()
        return [line.split()[0] for line in output.splitlines() if ' connected' in line]
    except Exception as e:
        print(f"Error getting Xorg displays: {e}")
        return []

def create_pipeline(display, width, height, env):
    pipeline_desc = (
        "v4l2src device=/dev/video61 ! "
        "videoconvert ! "
        "videoscale ! "
    )

    if env == 'WAYLAND':
        pipeline_desc += f"waylandsink render-rectangle=\"<0,0,{width},{height}>\""
    elif env == 'XORG':
        pipeline_desc += "ximagesink"  # Adjust for X11 as needed

    pipeline = Gst.parse_launch(pipeline_desc)
    pipeline.set_state(Gst.State.PLAYING)
    return pipeline

def main():
    Gst.init(None)
    pipelines = []

    if is_wayland():
        displays = get_wayland_displays()
        env = 'WAYLAND'
    elif is_xorg():
        displays = get_xorg_displays()
        env = 'XORG'
    else:
        print("Neither Wayland nor Xorg display server detected.")
        sys.exit(1)

    for display in displays:
        if env == 'WAYLAND':
            _, width, height = display
            pipeline = create_pipeline(display[0], width, height, env)
        else:
            pipeline = create_pipeline(display, 0, 0, env)
        pipelines.append(pipeline)

    # Start the main loop
    mainloop = GLib.MainLoop()
    try:
        mainloop.run()
    except KeyboardInterrupt:
        for pipeline in pipelines:
            pipeline.set_state(Gst.State.NULL)
        print("\nStopped playback")

if __name__ == "__main__":
    main()
