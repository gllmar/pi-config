#!/bin/bash

# Function to list v4l2 video inputs
list_v4l2_inputs() {
    echo "Available V4L2 Video Inputs:"
    v4l2-ctl --list-devices | grep -E '(/dev/video[0-9]+)'
}

# Function to list ALSA audio inputs
list_alsa_inputs() {
    echo "Available ALSA Audio Inputs:"
    arecord -l
}

# Default values
default_video_device="/dev/video61"
default_audio_device="hw:4,0"
service_file="$HOME/.config/systemd/user/gst-loopback-ndi-send.service"

# Ask the user if they want to install or uninstall the service
read -p "Do you want to install the service? (y/n): " install_choice
if [[ "$install_choice" =~ ^[Yy]$ ]]; then
    # Menu for selecting video input
    echo "Select V4L2 Video Input:"
    list_v4l2_inputs
    read -p "Enter choice or press enter for default [$default_video_device]: " video_choice
    video_device=${video_choice:-$default_video_device}

    # Menu for selecting audio input
    echo "Select ALSA Audio Input:"
    list_alsa_inputs
    read -p "Enter device name and number in the format 'hw:X,Y' or press enter for default [$default_audio_device]: " audio_choice
    audio_device=${audio_choice:-$default_audio_device}

    # Creating the service file
    mkdir -p "$(dirname "$service_file")"

    cat << EOF > "$service_file"
[Unit]
Description=GStreamer Service

[Service]
Type=simple
ExecStart=/usr/bin/gst-launch-1.0 -v v4l2src device=$video_device ! videoconvert ! ndisinkcombiner name=combiner alsasrc device="$audio_device" ! audioconvert ! combiner.audio combiner.src ! queue ! ndisink ndi-name="My NDI source"

[Install]
WantedBy=default.target
EOF

    # Enable and start the service
    systemctl --user daemon-reload
    systemctl --user enable gst-service.service
    systemctl --user start gst-service.service

    echo "GStreamer service created and started."
elif [[ "$install_choice" =~ ^[Nn]$ ]]; then
    read -p "Do you want to uninstall the service? (y/n): " uninstall_choice
    if [[ "$uninstall_choice" =~ ^[Yy]$ ]]; then
        # Stop and disable the service
        systemctl --user stop gst-service.service
        systemctl --user disable gst-service.service

        # Remove the service file
        rm -f "$service_file"

        echo "GStreamer service has been uninstalled."
    else
        echo "No changes made."
    fi
else
    echo "Invalid input. Exiting."
fi
