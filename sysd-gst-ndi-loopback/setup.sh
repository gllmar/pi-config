#!/bin/bash

# Define the configuration directory
CONFIG_DIR="${HOME}/.config/gst-ndi-loopback"
mkdir -p "${CONFIG_DIR}"


# Execution script path
EXEC_SCRIPT="${CONFIG_DIR}/gst-ndi-exec.sh"

# Ask user for configuration
read -p "Enter NDI source filter: " FILTER_STRING
read -p "Enter loopback device ID [62]: " LOOPBACK_DEVICE
LOOPBACK_DEVICE=${LOOPBACK_DEVICE:-62}
read -p "Enter NDI width [1280]: " NDI_WIDTH
NDI_WIDTH=${NDI_WIDTH:-1280}
read -p "Enter NDI height [720]: " NDI_HEIGHT
NDI_HEIGHT=${NDI_HEIGHT:-720}
read -p "Enable fullscreen mode? (y/N): " FULLSCREEN_ENABLED
FULLSCREEN_ENABLED=${FULLSCREEN_ENABLED,,} # Convert to lowercase
FULLSCREEN_ENABLED=${FULLSCREEN_ENABLED:="false"}
if [[ "$FULLSCREEN_ENABLED" == "y" ]]; then
    FULLSCREEN_ENABLED="true"
else
    FULLSCREEN_ENABLED="false"
fi

# Save configuration to an environment file
ENV_FILE="${CONFIG_DIR}/${LOOPBACK_DEVICE}.env"
echo "FILTER_STRING=${FILTER_STRING}" > "${ENV_FILE}"
echo "LOOPBACK_DEVICE=${LOOPBACK_DEVICE}" >> "${ENV_FILE}"
echo "NDI_WIDTH=${NDI_WIDTH}" >> "${ENV_FILE}"
echo "NDI_HEIGHT=${NDI_HEIGHT}" >> "${ENV_FILE}"
echo "FULLSCREEN_ENABLED=${FULLSCREEN_ENABLED}" >> "${ENV_FILE}"

# Deploy the execution script
cat > "${EXEC_SCRIPT}" <<'EOF'
#!/bin/bash
source "${1}"

# Use environment variables if set, else use default values
loopback_device=${LOOPBACK_DEVICE:-"62"}
filter_string=${FILTER_STRING:-""}
ndi_width=${NDI_WIDTH:-"1280"}  # Default width
ndi_height=${NDI_HEIGHT:-"720"} # Default height
fullscreen_enabled=${FULLSCREEN_ENABLED:"false"}  # Default to false


# Function to log incoming resolution
log_incoming_resolution() {
    local ndi_url="$1"

    echo "Checking incoming video resolution..."

    # Launch a pipeline that outputs the caps, then extract the video resolution from caps
    local caps=$(timeout 1s gst-launch-1.0 -v ndisrc url-address="$ndi_url" ! ndisrcdemux name=demux demux.video ! queue ! fakesink 2>&1 | grep -o 'width=(int)[0-9]*, height=(int)[0-9]*')

    if [[ ! -z "$caps" ]]; then
        # Extract the resolution using Perl-compatible regex
        local width=$(echo "$caps" | grep -oP 'width=\(int\)\K[0-9]*' | head -n 1)
        local height=$(echo "$caps" | grep -oP 'height=\(int\)\K[0-9]*' | head -n 1)
        
        if [[ ! -z "$width" && ! -z "$height" ]]; then
            local resolution="${width}x${height}"
            echo "Incoming video resolution detected: $resolution"
        else
            echo "Could not determine incoming video resolution."
        fi
    else
        echo "Could not determine incoming video resolution."
    fi
}



# Function to start the pipeline
start_pipeline() {
    local ndi_url="$1"
    local ndi_width="$2"
    local ndi_height="$3"
    local loopback_device="$4"

    # Construct the GStreamer pipeline
    local pipeline="gst-launch-1.0 ndisrc url-address=$ndi_url ! ndisrcdemux name=demux demux.video ! queue ! videoconvert ! videoscale ! video/x-raw,format=YUY2,width=$ndi_width,height=$ndi_height ! v4l2sink device=/dev/video$loopback_device"    
    echo "Starting pipeline: $pipeline"
    eval "$pipeline" &

    local pipeline_pid=$!

    # Start ffplay if fullscreen is enabled
    if $fullscreen_enabled; then
        ffplay -f v4l2 -i /dev/video$loopback_device >/dev/null 2>&1 &
        local ffplay_pid=$!
    fi

    wait $pipeline_pid
    kill $ffplay_pid 2>/dev/null
}

# Extract information about all NDI sources
ndi_sources_info=$(timeout 2s gst-device-monitor-1.0 -f Source/Network:application/x-ndi 2>/dev/null)

# Find the first matching NDI source based on filter string and extract its name and URL
ndi_name=$(echo "$ndi_sources_info" | grep -B 4 "$filter_string" | grep "ndi-name" | head -n 1 | cut -d "=" -f2- | tr -d "[:space:]")
ndi_url=$(echo "$ndi_sources_info" | grep -A 1 "$filter_string" | grep "url-address" | head -n 1 | cut -d "=" -f2- | tr -d "[:space:]")

# Check if NDI name and URL were found
if [ -z "$ndi_url" ] || [ -z "$ndi_name" ]; then
    echo "No matching NDI source found."
    exit 1
fi

# Print the complete name of the found NDI stream
echo "Found NDI stream: $ndi_name with URL: $ndi_url"

# Add a call to log_incoming_resolution before starting the main pipeline
log_incoming_resolution "$ndi_url"

# Start the pipeline with the specified resolution
start_pipeline "$ndi_url" "$ndi_width" "$ndi_height" "$loopback_device"
EOF
chmod +x "${EXEC_SCRIPT}"

# Create systemd user service file
SERVICE_FILE="${HOME}/.config/systemd/user/gst-ndi-loopback@.service"
cat > "${SERVICE_FILE}" <<EOF
[Unit]
Description=GStreamer NDI Loopback Service for /dev/video%I

[Service]
Type=simple
EnvironmentFile=${CONFIG_DIR}/%i.env
ExecStart=${EXEC_SCRIPT} ${CONFIG_DIR}/%i.env

[Install]
WantedBy=default.target
EOF

# Reload systemd daemon and enable service
systemctl --user daemon-reload
systemctl --user enable gst-ndi-loopback@${LOOPBACK_DEVICE}.service

echo "Setup complete. Service enabled as gst-ndi-loopback@${LOOPBACK_DEVICE}.service"
