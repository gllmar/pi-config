#!/bin/bash

# Ensure the script is run as root
if [ "$(id -u)" -ne 0 ]; then
    echo "Please run as root."
    exit 1
fi

# Function to install necessary packages
install_avahi_packages() {
    if ! dpkg -l | grep -q avahi-daemon; then
        echo "Installing Avahi daemon and utilities..."
        apt-get update
        apt-get install -y avahi-daemon avahi-utils
        if [ $? -ne 0 ]; then
            echo "Failed to install Avahi packages. Exiting."
            exit 1
        fi
    else
        echo "Avahi is already installed. Proceeding..."
    fi
}

# Function to get user's icon selection
get_icon_selection() {
    icons=("PowerBook" "PowerMac" "Macmini" "iMac" "MacBook" "MacBookPro" "MacBookAir" "MacPro" "AppleTV1,1" "AirPort")
    echo "Select an icon by number (default: 10 - AirPort):"
    for i in "${!icons[@]}"; do
        echo "$((i+1)). ${icons[$i]}"
    done

    read -r selection
    if [ -z "$selection" ]; then
        selection=10
    fi

    if ! [[ $selection =~ ^[0-9]+$ ]] || [ "$selection" -lt 1 ] || [ "$selection" -gt 10 ]; then
        echo "Invalid selection. Exiting."
        exit 1
    fi
    icon=${icons[$((selection-1))]}
    echo "You've selected: $icon"
}

# Function to write service file
write_service_file() {
    service_file="/etc/avahi/services/discovery.service"
    if [ -f "$service_file" ]; then
        read -p "$service_file already exists. Overwrite? (y/N) " overwrite
        if [[ ! "$overwrite" =~ ^[yY]$ ]]; then
            echo "Exiting without overwriting."
            exit 0
        fi
    fi

    echo "Writing service definitions to $service_file..."

    cat > "$service_file" <<EOF
<?xml version="1.0" standalone='no'?>
<!DOCTYPE service-group SYSTEM "avahi-service.dtd">
<service-group>
  <name replace-wildcards="yes">%h</name> 
  <service>
    <type>_smb._tcp</type>
    <port>445</port>
  </service>
  <service>
    <type>_ssh._tcp</type>
    <port>22</port>
  </service>
  <service>
    <type>_device-info._tcp</type>
    <port>0</port>
    <txt-record>model=$icon</txt-record>
  </service>
</service-group>
EOF
}

# Function to restart Avahi daemon
restart_avahi() {
    echo "Restarting Avahi daemon..."
    systemctl restart avahi-daemon
    if [ $? -ne 0 ]; then
        echo "Failed to restart Avahi daemon. Exiting."
        exit 1
    fi
}

# Main script execution
install_avahi_packages
get_icon_selection
write_service_file
restart_avahi
echo "Avahi configuration completed!"
