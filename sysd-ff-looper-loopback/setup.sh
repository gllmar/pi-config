#!/bin/bash

# Function to check and install dependencies
install_dependencies() {
    local dependencies=("dialog" "ffmpeg" "v4l2loopback-dkms")
    local is_missing=0

    echo "Checking for required dependencies..."

    for dep in "${dependencies[@]}"; do
        if ! dpkg -s "$dep" &> /dev/null; then
            echo "$dep is not installed."
            is_missing=1
        fi
    done

    # Check if v4l2loopback kernel module is loaded
    if ! lsmod | grep -q v4l2loopback; then
        echo "v4l2loopback kernel module is not loaded."
        is_missing=1
    fi

    if [[ $is_missing -eq 1 ]]; then
        echo "Installing missing dependencies..."
        sudo apt-get update
        sudo apt-get install -y "${dependencies[@]}"
        sudo modprobe v4l2loopback
    else
        echo "All dependencies are installed and v4l2loopback is loaded."
    fi
}

# Function to browse for a file using dialog
browse_file() {
    local config_path=$1
    local initial_dir="${HOME}/Videos/"
    local file_path=$(dialog --title "Select Video File" --fselect "${initial_dir}" 10 60 3>&1 1>&2 2>&3)
    exit_status=$?
    clear

    if [ $exit_status = 0 ]; then
        if [ ! -f "$file_path" ]; then
            echo "Error: Selected file does not exist."
            exit 1
        fi
        echo "FILE_PATH=${file_path}" > "${config_path}"
        echo "Config file created at ${config_path}"
    else
        echo "File selection cancelled."
        exit 1
    fi
}

# Function to create a systemd user service
create_service() {
    local instance=$1
    local config_path=$2
    local restart_hyperhdr=$3
    local service_file="${HOME}/.config/systemd/user/ff-looper-${instance}.service"

    if [ ! -e "/dev/video${instance}" ]; then
        echo "Error: /dev/video${instance} does not exist. Ensure v4l2loopback is loaded."
        exit 1
    fi

    echo "Creating systemd user service for /dev/video${instance} at ${service_file}"

    # Create the systemd service file
    cat <<EOF > "${service_file}"
[Unit]
Description=FFmpeg Loopback Service for /dev/video${instance}
After=hyperhdr.service  # Ensure HyperHDR starts before this service

[Service]
Type=simple
EnvironmentFile=${config_path}
ExecStart=/usr/bin/ffmpeg -stream_loop -1 -re -i \${FILE_PATH} -f v4l2 /dev/video${instance}
$( [[ "$restart_hyperhdr" == "y" ]] && echo "ExecStartPost=/bin/bash -c 'sleep 10; systemctl --user restart hyperhdr'" )
[Install]
WantedBy=default.target
EOF

    echo "Service file created at: ${service_file}"

    # Reload the systemd user daemon
    systemctl --user daemon-reload
    echo "Systemd daemon reloaded."

    # Prompt to start or restart the service
    read -p "Do you want to start or restart the service now? (y/n, default: y) " answer
    answer=${answer:-y}
    if [[ "$answer" =~ ^[Yy]$ ]]; then
        systemctl --user restart ff-looper-${instance}
        echo "Service started/restarted for /dev/video${instance}."
    fi

    # Prompt to enable the service at boot
    read -p "Do you want to enable the service to start at boot? (y/n, default: y) " boot_answer
    boot_answer=${boot_answer:-y}
    if [[ "$boot_answer" =~ ^[Yy]$ ]]; then
        systemctl --user enable ff-looper-${instance}
        echo "Service enabled to start at boot for /dev/video${instance}."
    fi
}

# Main script logic
echo "FFmpeg Loopback Service Setup"
echo "----------------------------"

install_dependencies

# Prompt for the instance number
read -p "Enter the loopback device number (default 61, range 61-68): " instance
instance=${instance:-61}  # Default to 61 if no input is given

# Validate instance number
if [[ $instance -lt 61 ]] || [[ $instance -gt 68 ]]; then
    echo "Invalid loopback device number. Please enter a number between 61 and 68."
    exit 1
fi

echo "Selected loopback device: /dev/video${instance}"

# Create config directory if it doesn't exist
config_dir="${HOME}/.config/ff-looper-loopback"
mkdir -p "${config_dir}"

# Check if config already exists
config_path="${config_dir}/${instance}"
if [ -f "${config_path}" ]; then
    echo "Configuration file detected: ${config_path}"
    cat "${config_path}"
    read -p "Do you want to keep the existing configuration? (y/n, default: y) " preserve_config
    preserve_config=${preserve_config:-y}

    if [[ "$preserve_config" =~ ^[Yy]$ ]]; then
        echo "Preserving existing configuration."
    else
        echo "Creating a new configuration file..."
        browse_file "${config_path}"
    fi
else
    echo "No configuration file detected. Creating a new one..."
    browse_file "${config_path}"
fi

# Ask if the user wants to add a restart command for HyperHDR
read -p "Do you want to add a command to restart HyperHDR after the loop starts? (y/n, default: y) " hyperhdr_answer
hyperhdr_answer=${hyperhdr_answer:-y} # Default to yes if no input is given
hyperhdr_answer=${hyperhdr_answer,,} # Convert to lowercase

# Create the user service
create_service "$instance" "$config_path" "$hyperhdr_answer"
