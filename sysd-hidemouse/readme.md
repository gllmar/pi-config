


cat hide-mouse.service 
[Unit]
Description=Hide mouse cursor service
After=wait-for-wf-panel-pi.service
Wants=wait-for-wf-panel-pi.service

[Service]
Type=oneshot
ExecStart=/bin/bash -c ' \
  DEVICE="/dev/input/event0"; \
  evemu-event $DEVICE --type EV_ABS --code ABS_MT_POSITION_X --value 100000; \
  evemu-event $DEVICE --type EV_ABS --code ABS_MT_POSITION_Y --value 100000; \
  evemu-event $DEVICE --type EV_ABS --code ABS_MT_TRACKING_ID --value 1; \
  evemu-event $DEVICE --type EV_KEY --code BTN_TOUCH --value 1; \
  evemu-event $DEVICE --type EV_SYN --code SYN_REPORT --value 0; \
  evemu-event $DEVICE --type EV_ABS --code ABS_MT_TRACKING_ID --value -1; \
  evemu-event $DEVICE --type EV_KEY --code BTN_TOUCH --value 0; \
  evemu-event $DEVICE --type EV_SYN --code SYN_REPORT --value 0'

[Install]
WantedBy=default.target
