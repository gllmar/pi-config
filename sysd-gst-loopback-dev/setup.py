import subprocess
import signal
import sys
import re
import os

def run_command(command):
    try:
        return subprocess.check_output(command, shell=True).decode('utf-8')
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {e}")
        return ""

def select_option(options, prompt):
    for i, option in enumerate(options, start=1):
        print(f"{i}. {option}")
    while True:
        try:
            choice = int(input(prompt)) - 1
            if 0 <= choice < len(options):
                return options[choice]
            else:
                print("Invalid selection. Please try again.")
        except ValueError:
            print("Invalid input. Please enter a number.")

def get_input(prompt, default_value):
    user_input = input(prompt).strip()
    return user_input if user_input else default_value

def set_dbus_session():
    user = os.environ.get('USER')
    try:
        dbus_address = subprocess.check_output(f"sudo -u {user} printenv DBUS_SESSION_BUS_ADDRESS", shell=True).decode().strip()
        if dbus_address:
            os.environ['DBUS_SESSION_BUS_ADDRESS'] = dbus_address
        else:
            # Default DBUS_SESSION_BUS_ADDRESS
            os.environ['DBUS_SESSION_BUS_ADDRESS'] = "unix:path=/run/user/1000/bus"
    except subprocess.CalledProcessError:
        print("DBUS_SESSION_BUS_ADDRESS not found. Setting to default: unix:path=/run/user/1000/bus")
        os.environ['DBUS_SESSION_BUS_ADDRESS'] = "unix:path=/run/user/1000/bus"

def create_systemd_service(name, command):
    service_template = f"""[Unit]
Description=GStreamer Loopback Service for {name}
After=network.target

[Service]
Type=simple
ExecStart=/bin/bash -c '{command}'
Restart=always
RestartSec=5

[Install]
WantedBy=default.target
"""
    service_path = os.path.expanduser(f"~/.config/systemd/user/{name}.service")
    with open(service_path, "w") as service_file:
        service_file.write(service_template)
    subprocess.call(["systemctl", "--user", "daemon-reload"])
    return service_path

def enable_and_start_service(name):
    subprocess.call(["systemctl", "--user", "enable", name])
    subprocess.call(["systemctl", "--user", "start", name])

# Signal handler for gracefully stopping the pipeline
def signal_handler(sig, frame):
    print('\nExiting...')
    sys.exit(0)

# Register the signal handler
signal.signal(signal.SIGINT, signal_handler)

# List available devices
devices_output = run_command("v4l2-ctl --list-devices")
devices = re.findall('(.*):\n\t(/dev/video\d+)', devices_output)
selected_device = select_option([f"{d[0]} ({d[1]})" for d in devices], "Select a device: ")

# Use the correct device path for the selected device
device_path = devices[[f"{d[0]} ({d[1]})" for d in devices].index(selected_device)][1].strip()
modes_output = run_command(f"v4l2-ctl --device={device_path} --list-formats-ext")

# Parsing for capture modes, resolutions, and frame rates
modes = re.findall("'(\S+)' \((.+?)\)(.+?)(?=\n\s+\n|\Z)", modes_output, re.DOTALL)
selected_mode_info = select_option([f"{m[1]} ({m[0]})" for m in modes], "Select a capture mode: ")
selected_mode_format = selected_mode_info.split(' ')[-1].strip('()')
selected_mode_name = ' '.join(selected_mode_info.split(' ')[:-1]).strip('()')

# Find the selected mode's details for parsing resolutions and frame rates
for mode in modes:
    if mode[0] == selected_mode_format and mode[1].startswith(selected_mode_name):
        mode_details = mode[2]
        break

resolution_fps_pairs = re.findall('Size: Discrete (\d+x\d+)(.+?)(?=Size: Discrete|\Z)', mode_details, re.DOTALL)
all_options = []
for res, fps_details in resolution_fps_pairs:
    fps_values = re.findall('Interval: .*?([\d.]+) fps', fps_details)
    for fps in fps_values:
        all_options.append((res, fps))

# Ensure that all_options is not empty
if not all_options:
    print("No resolutions and frame rates found for this mode.")
    sys.exit(1)

# Let the user select a resolution and frame rate
selected_resolution_info = select_option([f"{r[0]} at {r[1]} fps" for r in all_options], "Select a resolution and frame rate: ")
selected_resolution, selected_frame_rate = selected_resolution_info.split(' at ')
frame_rate_numerator = selected_frame_rate.split('.')[0]  # Extracts '30' from '30.000'

# Ask user for loopback device and count
default_loopback_device = "/dev/video61"
loopback_device = get_input(f"Enter loopback device (default: {default_loopback_device}): ", default_loopback_device)

default_loopback_count = 1
loopback_count = int(get_input(f"Enter number of loopback devices (default: {default_loopback_count}): ", default_loopback_count))

# Construct the GStreamer pipeline command
selected_resolution_width, selected_resolution_height = selected_resolution.split('x')
gst_pipeline = [
    "gst-launch-1.0",
    f"v4l2src device={device_path}",
    f"! video/x-raw,width={selected_resolution_width},height={selected_resolution_height},framerate={frame_rate_numerator}/1",
    "! videoconvert",
    "! tee name=t"
]

# Append loopback sinks to the pipeline
for i in range(loopback_count):
    loopback_device_path = loopback_device[:-1] + str(int(loopback_device[-1]) + i) if loopback_count > 1 else loopback_device
    gst_pipeline.append(f"t. ! queue ! v4l2sink device={loopback_device_path}")

gst_command = " ".join(gst_pipeline)

def main():
    # Show the GStreamer pipeline command
    print("GStreamer Pipeline Command:")
    print(gst_command)
    
    # Ask if the user wants to create a systemd user service with this command
    create_service = get_input("Do you want to create a systemd user service with this command? (Y/n): ", "Y").lower()
    if create_service in ["yes", "y"]:
        set_dbus_session()
        video_index = device_path.split('/')[-1].replace('video', '')  # Extract index from /dev/videoX
        service_name = f"loopback-dev-{video_index}"
        service_path = create_systemd_service(service_name, gst_command)
        print(f"Systemd service created at {service_path}")

        enable_service = get_input("Do you want to enable this service at boot? (Y/n): ", "Y").lower()
        if enable_service in ["yes", "y"]:
            enable_and_start_service(service_name)
            print(f"Service {service_name} enabled and started.")

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("\nExiting...")
