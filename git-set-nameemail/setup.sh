#!/bin/bash

# Get the computer name
computer_name=$(hostname)

# Get the current user's username
user=$(whoami)

# Preview the new configuration
echo "Preview of Git configuration:"
echo "Name: $computer_name"
echo "Email: $user@$computer_name.no"

# Ask the user for confirmation
read -p "Do you want to set this as your Git configuration? (y/n) " -n 1 -r
echo    # move to a new line

if [[ $REPLY =~ ^[Yy]$ ]]
then
    # Set the Git global configuration for user name and email
    git config --global user.name "$computer_name"
    git config --global user.email "$user@$computer_name.no"
    echo "Git configuration updated."
else
    echo "No changes made."
fi
