#!/bin/bash

# Configuration variables
CONDA_ENV_NAME="iopaint_env"
CONFIG_FILE="$HOME/iopaint-config.json"
SERVICE_NAME="iopaint.service"
IOPAINT_PORT=8085  # Default port set to 8085

# Function to install dependencies
install_dependencies() {
    echo "Installing Conda and PyTorch dependencies..."

    # Check if Conda is installed
    if ! command -v conda &> /dev/null; then
        echo "Conda is not installed. Please install Conda and rerun the script."
        exit 1
    fi

    # Create the Conda environment
    conda create -n $CONDA_ENV_NAME python=3.10 -y

    # Activate the Conda environment
    source $(conda info --base)/etc/profile.d/conda.sh
    conda activate $CONDA_ENV_NAME

    # Install PyTorch
    read -p "Do you want to use NVIDIA GPU (y/n)? [y]: " use_nvidia
    use_nvidia=${use_nvidia:-y}  # Default to "y" if no input

    if [[ $use_nvidia =~ ^[Yy]$ ]]; then
        echo "Installing PyTorch with NVIDIA GPU support..."
        pip install torch==2.1.2 torchvision==0.16.2 --index-url https://download.pytorch.org/whl/cu118
    else
        read -p "Do you want to use AMD GPU (only on Linux) (y/n)? [y]: " use_amd
        use_amd=${use_amd:-y}  # Default to "y" if no input

        if [[ $use_amd =~ ^[Yy]$ ]]; then
            echo "Installing PyTorch with AMD GPU support..."
            pip install torch==2.1.2 torchvision==0.16.2 --index-url https://download.pytorch.org/whl/rocm5.6
        else
            echo "Installing PyTorch without GPU support..."
            pip install torch==2.1.2 torchvision==0.16.2
        fi
    fi

    # Install IOPaint and additional dependencies
    echo "Installing IOPaint and additional dependencies..."
    pip install iopaint iopath

    # Create a default configuration file if it doesn't exist
    if [[ ! -f $CONFIG_FILE ]]; then
        echo "Creating a default configuration file..."
        echo '{}' > $CONFIG_FILE
    fi
}

# Function to configure and enable the systemd service
configure_systemd_service() {
    echo "Configuring systemd service for IOPaint..."

    # Create the systemd service file
    SERVICE_PATH="$HOME/.config/systemd/user/$SERVICE_NAME"
    mkdir -p "$(dirname "$SERVICE_PATH")"
    cat <<EOL > "$SERVICE_PATH"
[Unit]
Description=IOPaint Service
After=network.target

[Service]
ExecStart=$(which iopaint) start --config $CONFIG_FILE --device=cpu --host=0.0.0.0 --port=$IOPAINT_PORT
Restart=always
Environment="PATH=$CONDA_PREFIX/bin:$PATH"

[Install]
WantedBy=default.target
EOL

    # Reload the systemd daemon, enable, and start the service
    systemctl --user daemon-reload
    systemctl --user enable $SERVICE_NAME
    systemctl --user restart $SERVICE_NAME

    echo "IOPaint service enabled and started. Access it at http://0.0.0.0:$IOPAINT_PORT"
}

# Function to uninstall IOPaint
uninstall_iopaint() {
    echo "Uninstalling IOPaint..."
    conda remove -n $CONDA_ENV_NAME --all -y
    rm -f "$HOME/.config/systemd/user/$SERVICE_NAME"
    echo "IOPaint and its Conda environment have been removed."
}

# Ask the user if they want to install or uninstall IOPaint
read -p "Do you want to install IOPaint? (y/n): [y] " -n 1 -r
echo
REPLY=${REPLY:-y}  # Default to "y" if no input
if [[ $REPLY =~ ^[Yy]$ ]]; then
    install_dependencies
    configure_systemd_service
else
    read -p "Do you want to uninstall IOPaint? (y/n): [y] " -n 1 -r
    echo
    REPLY=${REPLY:-y}  # Default to "y" if no input
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        uninstall_iopaint
    else
        echo "No changes made."
    fi
fi
