```
name="v4l2loopback-conf"; script="setup-$name.sh"; curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script && sudo bash $script && rm $script
```


```
sudo apt-get install v4l2loopback-dkms
```

sudo modprobe v4l2loopback devices=3

```
sudo apt update && sudo apt install raspberrypi-kernel-headers
curl -LO https://github.com/umlaeute/v4l2loopback/archive/refs/tags/v0.12.7.tar.gz
tar xzf v0.12.7.tar.gz && cd v4l2loopback-0.12.7
make && sudo make install
sudo depmod -a
```

# to load it at boot
```
echo "v4l2loopback" | sudo tee -a /etc/modules
```



# to create multiple
```
sudo modprobe v4l2loopback devices=3
```

# to list device
```
v4l2-ctl --list-devices
```



# at boot

```
sudo nano /etc/modules-load.d/v4l2loopback.conf
```
```
v4l2loopback
```

```
sudo nano /etc/modprobe.d/v4l2loopback-options.conf
```

```
options v4l2loopback devices=8 card_label="loopback 1","loopback 2", "loopback 3", "loopback 4", "loopback 5","loopback 6","loopback 7","loopback 8","loopback 9"  video_nr=61 62 63 64 65 66 67 68 69
```


test : 

 gst-launch-1.0 videotestsrc ! video/x-raw,width=1280,height=50 ! tee name=t   t. ! queue ! videoconvert ! v4l2sink device=/dev/video61   t. ! queue ! videoconvert ! fbdevsink


gst-launch-1.0 videotestsrc pattern=ball  ! videoconvert ! video/x-raw,width=1280,height=50,framerate=60/1 ! tee name=t   t. ! queue ! videoconvert ! v4l2sink device=/dev/video61   t. ! queue ! videoconvert ! fbdevsink