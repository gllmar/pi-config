#!/bin/bash

INSTALL_DIR="/etc/modprobe.d/"
MODULE_LOAD_DIR="/etc/modules-load.d/"
MODULE_CONF="v4l2loopback.conf"
OPTIONS_CONF="v4l2loopback-options.conf"
MODULE_LOAD_CONTENT="v4l2loopback"

# Default configuration values
DEFAULT_DEVICES=8
DEFAULT_VIDEO_NR="61,62,63,64,65,66,67,68"
DEFAULT_LABELS="loopback_1,loopback_2,loopback_3,loopback_4,loopback_5,loopback_6,loopback_7,loopback_8"
DEFAULT_FPS=30
DEFAULT_WIDTH=1920
DEFAULT_HEIGHT=1080

# Check for v4l2loopback-dkms installation
if ! dpkg-query -W -f='${Status}' v4l2loopback-dkms 2>/dev/null | grep -q "ok installed"; then
    echo "v4l2loopback-dkms is not installed."
    read -p "Would you like to install it now? (y/n) " yn
    case $yn in
        [Yy]* )
            sudo apt-get update
            sudo apt-get install -y v4l2loopback-dkms
            ;;
        * )
            echo "Please install v4l2loopback-dkms and then run this script again."
            exit 1
            ;;
    esac
fi

# Function to install configuration
function install_config {
    read -p "Enter the number of devices to create (default: ${DEFAULT_DEVICES}): " devices
    devices=${devices:-$DEFAULT_DEVICES}

    read -p "Enter the video device numbers (default: ${DEFAULT_VIDEO_NR}): " video_nr
    video_nr=${video_nr:-$DEFAULT_VIDEO_NR}

    read -p "Enter the card labels (default: ${DEFAULT_LABELS}): " labels
    labels=${labels:-$DEFAULT_LABELS}

    read -p "Enter the default FPS (default: ${DEFAULT_FPS}): " fps
    fps=${fps:-$DEFAULT_FPS}

    read -p "Enter the resolution width (default: ${DEFAULT_WIDTH}): " width
    width=${width:-$DEFAULT_WIDTH}

    read -p "Enter the resolution height (default: ${DEFAULT_HEIGHT}): " height
    height=${height:-$DEFAULT_HEIGHT}

    OPTIONS_CONTENT="options v4l2loopback devices=${devices} card_label=\"${labels}\" video_nr=${video_nr} fps=${fps} width=${width} height=${height}"

    echo "Installing configurations..."
    echo "$MODULE_LOAD_CONTENT" | sudo tee "${MODULE_LOAD_DIR}${MODULE_CONF}" > /dev/null || {
        echo "Error writing to ${MODULE_LOAD_DIR}${MODULE_CONF}."
        exit 1
    }

    echo "$OPTIONS_CONTENT" | sudo tee "${INSTALL_DIR}${OPTIONS_CONF}" > /dev/null || {
        echo "Error writing to ${INSTALL_DIR}${OPTIONS_CONF}."
        exit 1
    }

    sudo modprobe v4l2loopback || {
        echo "Error: v4l2loopback module failed to load. Check your configuration."
        exit 1
    }

    echo "Configuration installed successfully."
}

# Function to uninstall configuration
function uninstall_config {
    echo "Uninstalling configurations..."
    sudo rm -f "${MODULE_LOAD_DIR}${MODULE_CONF}" || {
        echo "Error removing ${MODULE_LOAD_DIR}${MODULE_CONF}."
    }

    sudo rm -f "${INSTALL_DIR}${OPTIONS_CONF}" || {
        echo "Error removing ${INSTALL_DIR}${OPTIONS_CONF}."
    }

    echo "Configuration uninstalled successfully."
}

# Interactive menu
while true; do
    echo "Choose an option:"
    echo "1) Install configuration"
    echo "2) Uninstall configuration"
    echo "3) Exit"
    read -p "Enter your choice (1/2/3): " choice

    case $choice in
        1)
            install_config
            break
            ;;
        2)
            uninstall_config
            break
            ;;
        3)
            echo "Exiting."
            exit 0
            ;;
        *)
            echo "Invalid choice. Please enter 1, 2, or 3."
            ;;
    esac
done
