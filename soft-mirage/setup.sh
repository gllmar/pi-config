#!/bin/bash

# Define the Mirage GitLab repository URL
MIRAGE_REPO="https://gitlab.com/thomasross/mirage.git"
MIRAGE_DIR="/home/pi/src/mirage"
SHARE_DIR="/usr/share/mirage"

install_mirage() {
    # Clone the Mirage repository if it doesn't already exist
    if [ ! -d "$MIRAGE_DIR" ]; then
        mkdir -p ~/src
        cd ~/src
        git clone $MIRAGE_REPO
    fi

    # Change to the cloned directory
    cd $MIRAGE_DIR

    # Install required dependencies
    sudo apt-get update
    sudo apt-get install -y python3-gi python3-cairo libgexiv2-dev exiv2 libgtk-3-dev python3-dev libx11-dev gettext imagemagick

    # Install Mirage
    sudo python3 setup.py install

    # Copy necessary files
    sudo mkdir -p $SHARE_DIR
    sudo cp $MIRAGE_DIR/mirage_blank.png $SHARE_DIR
    sudo cp $MIRAGE_DIR/mirage.png $SHARE_DIR
    # Add other files if necessary

    echo "Mirage installation and desktop integration complete. You can run it using the 'mirage' command or find it in your applications menu."
}

uninstall_mirage() {
    echo "Uninstalling Mirage..."

    # Specific commands for uninstalling Mirage
    sudo rm -rf $SHARE_DIR
    # Add commands to remove other files and dependencies if necessary

    echo "Mirage has been uninstalled."
}

# Ask user if they want to install Mirage
read -p "Do you want to install Mirage? [Y/n]: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]] || [[ -z $REPLY ]]; then
    install_mirage
else
    # Ask user if they want to uninstall Mirage
    read -p "Do you want to uninstall Mirage? [y/N]: " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        uninstall_mirage
    else
        echo "No changes were made."
    fi
fi
