
```
name="soft-mirage"; script="setup-$name.sh"; curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script && bash $script && rm $script
```



custom command example (shrink long side to 1000 px, preserve ratio)
```
convert %F -thumbnail 1000x1000 %P%N_web.jpg
```