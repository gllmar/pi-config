#!/bin/bash

# Default installation directory
DEFAULT_INSTALL_DIR="$HOME/Applications"

# Function to install FreeCAD Ondsel
install_freecad() {
    local version="$1"
    local install_dir="$2"

    # Define URLs for FreeCAD releases and assets
    local releases_url="https://api.github.com/repos/Ondsel-Development/FreeCAD/releases"
    if [ "$version" = "latest" ]; then
        releases_url="$releases_url/latest"
    else
        releases_url="$releases_url/tags/$version"
    fi

    # Fetch release data
    local release_data=$(curl -s "$releases_url")

    # Extract download URL for the selected version and architecture
    local download_url=$(echo "$release_data" | jq -r '.assets[] | select(.name | endswith("-Linux-aarch64.AppImage")) | .browser_download_url')

    # Check if a download URL is available
    if [ -z "$download_url" ]; then
        echo "No compatible FreeCAD Ondsel package found for ARM64."
        return 1
    fi

    # Print the download URL for debugging
    echo "Download URL: $download_url"

    # Create installation directory
    mkdir -p "$install_dir"

    # Define the filename for the AppImage
    local appimage_file="$install_dir/FreeCAD-Ondsel-$version.AppImage"

    # Download FreeCAD Ondsel
    echo "Downloading FreeCAD Ondsel version $version..."
    wget --show-progress "$download_url" -O "$appimage_file"

    # Check if download was successful
    if [ ! -f "$appimage_file" ] || [ ! -s "$appimage_file" ]; then
        echo "Failed to download FreeCAD Ondsel. Please check the URL and try again."
        return 1
    fi

    # Make the AppImage executable
    chmod +x "$appimage_file"

    # Dependencies installation (Debian Bookworm)
    echo "Installing required dependencies..."
    sudo apt-get update
    sudo apt-get install -y libgl1 libfontconfig1 libxrender1 libsm6 libfreetype6 libxext6 fuse

    # Create desktop file
    local desktop_file="$install_dir/FreeCAD-Ondsel-$version.desktop"
    echo "[Desktop Entry]
Type=Application
Name=FreeCAD Ondsel $version
Exec=$appimage_file
Icon=freecad
Terminal=false
Categories=Graphics;3DGraphics;" > "$desktop_file"

    # Install desktop file
    sudo desktop-file-install "$desktop_file"

    # Download and set up the icon
    local icon_url="https://wiki.freecad.org/images/e/e0/Ondsel.svg"
    curl -s -o "$install_dir/ondsel-$version-icon.svg" "$icon_url"
    sudo cp "$install_dir/ondsel-$version-icon.svg" "/usr/share/pixmaps/ondsel-$version.svg"

    echo "FreeCAD Ondsel version $version installation complete."
}

# Main installation function
main() {
    # Check for jq installation
    if ! command -v jq &> /dev/null; then
        echo "jq could not be found, installing..."
        sudo apt-get update
        sudo apt-get install -y jq
    fi

    # Fetch available FreeCAD versions
    echo "Fetching available FreeCAD versions..."
    local versions_data=$(curl -s "https://api.github.com/repos/FreeCAD/FreeCAD/releases")
    mapfile -t freecad_versions < <(echo "$versions_data" | jq -r '.[].tag_name')

    # Menu to select version
    echo "Select a FreeCAD version to install (or 'latest' for the latest version):"
    select version in "latest" "${freecad_versions[@]}"; do
        echo "Enter the installation directory or leave blank for default ($DEFAULT_INSTALL_DIR):"
        read -r install_dir
        install_dir=${install_dir:-"$DEFAULT_INSTALL_DIR"}

        install_freecad "$version" "$install_dir"
        break
    done
}

# Run the main function
main
