#!/bin/bash

# Default USB device details (m5 atom)
default_vendor_id="0403"
default_product_id="6001"

# Function to list USB devices and let the user choose
select_usb_device() {
    echo "Listing USB devices..."
    devices=$(lsusb | grep -v "Linux Foundation" | awk '{print $6 " " $7 " " $8 " " $9 " " $10 " " $11 " " $12 " " $13}')
    IFS=$'\n'; devices=($devices); unset IFS

    echo "Available USB devices:"
    for i in "${!devices[@]}"; do
        echo "$((i+1))) ${devices[i]}"
    done

    read -p "Enter the number of the USB device you want to use (or press Enter to use the default device): " device_choice
    if [[ -n "$device_choice" && "$device_choice" =~ ^[0-9]+$ && "$device_choice" -le "${#devices[@]}" && "$device_choice" -gt 0 ]]; then
        selected_device=${devices[$((device_choice-1))]}
        vendor_id=$(echo "$selected_device" | cut -d':' -f1)
        product_id=$(echo "$selected_device" | cut -d' ' -f1 | cut -d':' -f2)
    else
        echo "Using default USB device."
        vendor_id=$default_vendor_id
        product_id=$default_product_id
    fi
}

# Function to install
install() {
    echo "Installing..."
    select_usb_device

    # Create udev rule
    udev_rule="ACTION==\"add\", SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"$vendor_id\", ATTRS{idProduct}==\"$product_id\", TAG+=\"systemd\", ENV{SYSTEMD_USER_WANTS}+=\"hyperhdr-usb-restart.service\""
    echo "$udev_rule" | sudo tee /etc/udev/rules.d/99-usb-autorestart.rules > /dev/null

    # Create systemd user service
    mkdir -p ~/.config/systemd/user
    echo -e "[Unit]\nDescription=Restart hyperhdr service on USB plug\n\n[Service]\nType=oneshot\nExecStart=/usr/bin/systemctl --user restart hyperhdr\n\n[Install]\nWantedBy=default.target" > ~/.config/systemd/user/hyperhdr-usb-restart.service

    # Reload udev rules and systemd
    sudo udevadm control --reload-rules && sudo udevadm trigger
    systemctl --user daemon-reload
    systemctl --user enable hyperhdr-usb-restart.service
    systemctl --user start hyperhdr-usb-restart.service
    echo "Installation complete."
}

# Function to uninstall
uninstall() {
    echo "Uninstalling..."
    sudo rm /etc/udev/rules.d/99-usb-autorestart.rules
    systemctl --user disable hyperhdr-usb-restart.service
    rm ~/.config/systemd/user/hyperhdr-usb-restart.service
    sudo udevadm control --reload-rules && sudo udevadm trigger
    systemctl --user daemon-reload
    echo "Uninstallation complete."
}

# Initial questions
echo "Do you want to install the service to restart 'hyperhdr' when a USB device is plugged in?"
read -p "Install? [Y/n]: " response
response=${response,,} # to lowercase
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]]; then
    install
else
    echo "Do you want to uninstall the service?"
    read -p "Uninstall? [y/N]: " response_uninstall
    response_uninstall=${response_uninstall,,} # to lowercase
    if [[ $response_uninstall =~ ^(yes|y) ]]; then
        uninstall
    else
        echo "No changes made."
    fi
fi
