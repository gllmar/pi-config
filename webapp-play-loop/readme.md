
play loop 1 file, give access folder via http interface 
```
 cvlc /home/pi/Videos/infloresences/ --input-repeat=99999999999 --fullscreen --no-osd --extraintf http --http-password framboise --http-port 9990
```

gst-launch-1.0 multifilesrc location="movie.mp4" loop=true ! qtdemux ! decodebin ! videoconvert ! autovideosink

sudo apt-get install gstreamer1.0-tools gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly




[Unit]
Description=VLC Loop Service
After=wait-for-wf-panel.service

[Service]
Environment=DISPLAY=:0
ExecStart=cvlc /home/pi/Videos/loops --input-repeat=99999999999 --fullscreen --no-osd --extraintf http --http-password framboise --http-port 9990
Restart=always
RestartSec=10

[Install]
WantedBy=default.target
