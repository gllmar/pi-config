import subprocess
import argparse
import time

def get_display_configuration():
    result = subprocess.run(['wlr-randr'], stdout=subprocess.PIPE, text=True)
    return result.stdout.strip()

def parse_display_configuration(config):
    monitors = {}
    current_monitor = None
    lines = config.split('\n')

    for line in lines:
        if line.strip().startswith('DSI') or line.strip().startswith('HDMI'):
            current_monitor = line.strip().split(' ')[0]
            monitors[current_monitor] = {}
        elif current_monitor:
            parts = line.strip().split(':')
            if len(parts) == 2:
                key = parts[0].strip()
                value = parts[1].strip()
                monitors[current_monitor][key] = value

    return monitors

def print_monitor_info(monitors):
    for monitor_name, info in monitors.items():
        print(f"{monitor_name} Enabled: {info.get('Enabled', 'No')}")
        print(f"Resolution: {info.get('Modes', 'N/A')}")
        print(f"Position: {info.get('Position', 'N/A')}")
        print("")

def monitor_display_changes():
    initial_info = get_display_configuration()
    print("Initial Monitor State:")
    initial_monitors = parse_display_configuration(initial_info)
    print_monitor_info(initial_monitors)

    while True:
        current_info = get_display_configuration()
        current_monitors = parse_display_configuration(current_info)

        if initial_info != current_info:
            print("Display configuration changed")
            print_monitor_info(current_monitors)
            initial_info = current_info

        time.sleep(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Monitor display changes.")
    args = parser.parse_args()
    print("Starting display monitor for changes...")
    monitor_display_changes()
