#!/bin/bash
export DISPLAY=${DISPLAY:-:0}
# Function to get current screen configuration using xrandr
get_screen_configuration() {
    xrandr | grep -E " connected" | awk '{ print $1, $3 }'
}

# Initialize variables
DEBUG_MODE=0
EXEC_SCRIPT=""

# Parse arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -d|--debug) DEBUG_MODE=1 ;;
        --script) EXEC_SCRIPT="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

# Function for debug print
debug_print() {
    if [ "$DEBUG_MODE" -eq 1 ]; then
        echo "$1"
    fi
}

# Function to handle potential display configuration change
handle_potential_display_change() {
    debug_print "Potential display configuration change detected"
    NEW_CONFIG=$(get_screen_configuration)
    
    if [ "$NEW_CONFIG" != "$CURRENT_CONFIG" ]; then
        debug_print "Display configuration changed"
        debug_print "Previous Configuration: $CURRENT_CONFIG"
        debug_print "Current Configuration: $NEW_CONFIG"
        
        # Execute the script if specified
        if [ -n "$EXEC_SCRIPT" ]; then
            bash "$EXEC_SCRIPT" $NEW_CONFIG
        else
            echo "Connected Displays and Resolutions:"
            echo "$NEW_CONFIG"
        fi

        # Update the current configuration
        CURRENT_CONFIG="$NEW_CONFIG"
    fi
}

# Get initial screen configuration
CURRENT_CONFIG=$(get_screen_configuration)

# Print initial screen configuration
echo "Initial Connected Displays and Resolutions:"
echo "$CURRENT_CONFIG"

# Main loop to listen for D-Bus messages
dbus-monitor --session "type='signal',interface='org.freedesktop.DBus.ObjectManager',member='InterfacesAdded'" \
             "type='signal',interface='org.freedesktop.DBus.ObjectManager',member='InterfacesRemoved'" |
while read -r line; do
    if [[ $line == *"InterfacesAdded"* ]] || [[ $line == *"InterfacesRemoved"* ]]; then
        handle_potential_display_change
    fi
done
