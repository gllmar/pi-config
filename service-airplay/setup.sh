#!/bin/bash


# Function to install dependencies for Shairport Sync
install_dependencies() {
    echo "Installing dependencies for Shairport Sync..."
    sudo apt update
    sudo apt upgrade -y # optional but recommended
    sudo apt install --no-install-recommends build-essential git autoconf automake libtool \
        libpopt-dev libconfig-dev avahi-daemon libavahi-client-dev libssl-dev libsoxr-dev libpipewire-0.3-dev \
        libplist-dev libsodium-dev libavutil-dev libavcodec-dev libavformat-dev uuid-dev libgcrypt-dev xxd  libasound2-dev -y
}

# Function to clone and build nqptp
build_nqptp_sync() {
    echo  " Cloning Nqptp"
    git clone https://github.com/mikebrady/nqptp.git ~/src/nqptp
    cd ~/src/nqptp
    autoreconf -fi  
    ./configure --with-systemd-startup  
    make
    sudo make install
    sudo systemctl daemon-reload
    sudo systemctl restart nqptp.service
    sudo systemctl enable nqptp.service
}

# Function to clone and build Shairport Sync
build_shairport_sync() {
    echo "Cloning Shairport Sync..."
    git clone https://github.com/mikebrady/shairport-sync.git ~/src/shairport-sync
    cd ~/src/shairport-sync

    echo "Building Shairport Sync..."
    autoreconf -fi
    ./configure --sysconfdir=/etc --with-alsa \
        --with-soxr --with-avahi --with-ssl=openssl --with-systemd --with-pw --with-airplay-2
    make

    echo "Installing Shairport Sync..."
    sudo make install
}

# Function to set up Shairport Sync as a user service
setup_user_service() {
    echo "Setting up Shairport Sync as a user service..."
    mkdir -p ~/.config/systemd/user/
    cat <<EOF > ~/.config/systemd/user/shairport-sync.service
[Unit]
Description=Shairport Sync AirPlay receiver

[Service]
ExecStart=/usr/local/bin/shairport-sync
Restart=always

[Install]
WantedBy=default.target
EOF

    systemctl --user enable shairport-sync
    systemctl --user start shairport-sync
    sudo loginctl enable-linger $USER
}

# Script execution starts here
echo "Shairport Sync Installation Script for Raspberry Pi (Debian Bookworm with PipeWire)"

install_dependencies
build_nqptp_sync
build_shairport_sync
setup_user_service

echo "Shairport Sync installation and setup complete."
