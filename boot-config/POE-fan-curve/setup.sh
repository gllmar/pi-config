#!/bin/bash

# Function to update a configuration file by adding or removing lines
update_config_file() {
    local operation="$1"
    local file="$2"
    local -n config_params=$3
    local line comment

    for line in "${!config_params[@]}"; do
        comment="${config_params[$line]}"
        if [ "$operation" = "add" ]; then
            if ! grep -qF -- "$line" "$file"; then
                echo "Adding '$line'"
                echo "# $comment" | sudo tee -a "$file" > /dev/null
                echo "$line" | sudo tee -a "$file" > /dev/null
            fi
        elif [ "$operation" = "remove" ]; then
            sudo sed -i "/$line/d" "$file"
        fi
    done
}

# PoE Fan Curve Configuration Parameters
declare -A poe_fan_config_params=(
    ["dtparam=poe_fan_temp0=50000"]="Set PoE hat fan temperature threshold to 50C"
    ["dtparam=poe_fan_temp1=60000"]="Set PoE hat fan temperature threshold to 60C"
    ["dtparam=poe_fan_temp2=70000"]="Set PoE hat fan temperature threshold to 70C"
    ["dtparam=poe_fan_temp3=80000"]="Set PoE hat fan temperature threshold to 80C"
)

# Main script for PoE Fan Curve Configuration
echo "Do you want to set a silent PoE fan curve? (Y/n)"
read choice

config_file="/boot/firmware/config.txt"

if [ "$choice" = "Y" ] || [ "$choice" = "y" ] || [ -z "$choice" ]; then
    update_config_file "add" "$config_file" poe_fan_config_params
    echo "Silent PoE fan curve set. Please reboot your Raspberry Pi."
else
    echo "Do you want to remove the silent PoE fan curve? (Y/n)"
    read remove_choice
    if [ "$remove_choice" = "Y" ] || [ "$remove_choice" = "y" ]; then
        update_config_file "remove" "$config_file" poe_fan_config_params
        echo "Silent PoE fan curve removed. Please reboot your Raspberry Pi."
    else
        echo "No changes made."
    fi
fi
