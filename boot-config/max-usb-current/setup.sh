#!/bin/bash

# Function to update a configuration file by adding or removing lines
# Arguments: 
#   1) operation (add or remove)
#   2) config file path
#   3) associative array of configuration parameters and comments
update_config_file() {
    local operation="$1"
    local file="$2"
    local -n config_params=$3  # Using a nameref for associative array
    local line comment

    for line in "${!config_params[@]}"; do
        comment="${config_params[$line]}"
        if [ "$operation" = "add" ]; then
            if ! grep -qF -- "$line" "$file"; then
                echo "Adding '$line'"
                echo "# $comment" | sudo tee -a "$file" > /dev/null
                echo "$line" | sudo tee -a "$file" > /dev/null
            fi
        elif [ "$operation" = "remove" ]; then
            sudo sed -i "/$line/d" "$file"
        fi
    done
}

# Configuration parameters for max_usb_current
declare -A max_usb_current_params=(
    ["max_usb_current=1"]="Increase USB current limit"
)

# Main script
echo "Do you want to increase the USB current limit? (Y/n for increase, anything else for normal)"
read user_choice

config_file="/boot/config.txt"

if [ "$user_choice" = "Y" ] || [ "$user_choice" = "y" ] || [ -z "$user_choice" ]; then
    update_config_file "add" "$config_file" max_usb_current_params
    echo "Increased USB current limit. Please reboot your Raspberry Pi."
else
    update_config_file "remove" "$config_file" max_usb_current_params
    echo "USB current limit set to normal. Please reboot your Raspberry Pi."
fi
