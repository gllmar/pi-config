
```
name="boot-config/max-usb-current"; script="setup-$(echo $name | tr '/' '-')".sh; correct_raw_url="https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh?ref_type=heads"; echo "Installation Name: $name"; echo "Local Script Filename: $script"; echo "Downloading Script From: $correct_raw_url"; curl -sSL "$correct_raw_url" -o "$script"; echo "Running script: $script"; bash "$script"; echo "Removing script: $script"; rm "$script"
```