#!/bin/bash

# Function to remove a line from config.txt
remove_config_line() {
    if grep -q "$1" /boot/config.txt; then
        echo "Would remove line from /boot/config.txt: $1"
        if [ "$DRY_RUN" = false ]; then
            echo "Removing line from /boot/config.txt: $1"
            sudo sed -i "/$1/d" /boot/config.txt
        fi
    fi
}

# Function to uninstall drivers and revert changes
uninstall() {
    echo "Uninstalling the drivers and reverting changes..."
    echo "This is ${DRY_RUN:+a dry run. No changes will be made.}${DRY_RUN:=not a dry run. Changes will be made.}"

    # Unload driver modules
    echo "Would unload driver modules..."
    if [ "$DRY_RUN" = false ]; then
        echo "Unloading driver modules..."
        sudo modprobe -r WS_xinchDSI_Touch
        sudo modprobe -r WS_xinchDSI_Screen
    fi

    # Remove driver files
    echo "Would remove driver files..."
    if [ "$DRY_RUN" = false ]; then
        echo "Removing driver files..."
        sudo rm /lib/modules/$(uname -r)/WS_xinchDSI_Touch.ko
        sudo rm /lib/modules/$(uname -r)/WS_xinchDSI_Screen.ko
        sudo rm /lib/modules/$(uname -r)/WS_xinchDSI_Touch_Interface.ko
        sudo rm /lib/modules/$(uname -r)/WS_xinchDSI_Screen_Interface.ko
    fi

    # Remove device tree overlays
    echo "Would remove device tree overlays..."
    if [ "$DRY_RUN" = false ]; then
        echo "Removing device tree overlays..."
        sudo rm /boot/overlays/WS_xinchDSI_Touch.dtbo
        sudo rm /boot/overlays/WS_xinchDSI_Screen.dtbo
    fi

    # Revert changes from config.txt
    echo "Would revert changes from /boot/config.txt..."
    remove_config_line "ignore_lcd=1"
    remove_config_line "dtoverlay=vc4-kms-v3d"
    remove_config_line "dtparam=i2c_vc=on"
    remove_config_line "dtparam=i2c_arm=on"
    remove_config_line "dtoverlay=WS_xinchDSI_Screen"
    remove_config_line "dtoverlay=WS_xinchDSI_Touch"

    # Update module dependencies
    echo "Would update module dependencies..."
    if [ "$DRY_RUN" = false ]; then
        echo "Updating module dependencies..."
        sudo depmod
    fi

    if [ "$DRY_RUN" = false ]; then
        echo "Uninstallation complete. A system reboot is recommended."
    else
        echo "Dry run complete. No changes have been made."
    fi
}

# Help function
show_help() {
    echo "Usage: $0 [option]"
    echo "Options:"
    echo "  --dry-run    Show what the script would do without making changes"
    echo "  --help       Display this help and exit"
    echo "  --uninstall  Uninstall the drivers and revert changes"
    echo ""
    echo "Example:"
    echo "  $0 --dry-run    # Perform a dry run and show what would be done"
    echo "  $0 --uninstall  # Actually uninstall everything"
}

# Initialize dry run as false by default
DRY_RUN=false

# Parse command line arguments
while [ "$#" -gt 0 ]; do
    case "$1" in
        --dry-run)
            DRY_RUN=true
            shift 1
            ;;
        --help)
            show_help
            exit 0
            ;;
        --uninstall)
            read -p "Are you sure you want to uninstall the drivers and revert all changes? [y/N]: " confirmation
            if [[ "$confirmation" =~ ^[Yy]$ ]]; then
                uninstall
            else
                echo "Uninstallation aborted."
            fi
            exit 0
            ;;
        *)
            show_help
            exit 1
            ;;
    esac
done

if [ "$DRY_RUN" = false ]; then
    # If no arguments are given, show help
    show_help
fi
