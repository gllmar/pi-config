#!/bin/bash

# Function to add or remove a line
update_config() {
    local operation="$1"
    local file="/boot/firmware/config.txt"
    local line comment

    for line in "${!config_params[@]}"; do
        comment="${config_params[$line]}"
        if [ "$operation" = "add" ]; then
            if ! grep -qF -- "$line" "$file"; then
                echo "Adding '$line'"
                echo "# $comment" | sudo tee -a "$file" > /dev/null
                echo "$line" | sudo tee -a "$file" > /dev/null
            fi
        elif [ "$operation" = "remove" ]; then
            sudo sed -i "/$line/d" "$file"
        fi
    done
}

# Associative array for configuration parameters and their comments
declare -A config_params=(
    ["dtparam=i2c_arm=on"]="Enable I2C ARM interface"
    ["dtoverlay=vc4-kms-v3d"]="Enable VC4 Kernel Mode Setting (KMS) driver"
    ["dtoverlay=vc4-kms-dsi-waveshare-panel,10_1_inch"]="Set up Waveshare 10.1 inch DSI display"
    ["dtparam=i2c_vc=on"]="Enable I2C interface on VideoCore"
)

# Main script
echo "Do you want to install touchscreen support? (Y/n)"
read install_choice

if [ "$install_choice" = "Y" ] || [ "$install_choice" = "y" ] || [ -z "$install_choice" ]; then
    update_config "add"
    echo "Installation complete. Please reboot your Raspberry Pi."
else
    echo "Do you want to uninstall touchscreen support? (Y/n)"
    read uninstall_choice
    if [ "$uninstall_choice" = "Y" ] || [ "$uninstall_choice" = "y" ]; then
        update_config "remove"
        echo "Uninstallation complete. Please reboot your Raspberry Pi."
    else
        echo "No changes made."
    fi
fi
