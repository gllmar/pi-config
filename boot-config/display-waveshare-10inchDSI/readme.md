```
name="boot-config/display-waveshare-10inchDSI"; script="setup-$(echo $name | tr '/' '-')".sh; correct_raw_url="https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh?ref_type=heads"; echo "Installation Name: $name"; echo "Local Script Filename: $script"; echo "Downloading Script From: $correct_raw_url"; curl -sSL "$correct_raw_url" -o "$script"; echo "Running script: $script"; bash "$script"; echo "Removing script: $script"; rm "$script"
```




Update: folded in main branch since april 2023?

https://github.com/raspberrypi/linux/blob/rpi-6.1.y/arch/arm/boot/dts/overlays/vc4-kms-dsi-waveshare-panel-overlay.dts
ref : https://github.com/waveshare/Waveshare-DSI-LCD/issues/4



```
dtparam=i2c_arm=on
dtoverlay=vc4-kms-v3d
dtoverlay=vc4-kms-dsi-waveshare-panel,10_1_inch
dtparam=i2c_vc=on
```




product

https://www.waveshare.com/10.1inch-dsi-lcd-c.htm

wiki 

https://www.waveshare.com/wiki/10.1inch_DSI_LCD_(C)


commands 

#Step 1: Download and enter the Waveshare-DSI-LCD driver folder
git clone https://github.com/waveshare/Waveshare-DSI-LCD
cd Waveshare-DSI-LCD
 
#Step 2: Enter uname -a in the terminal to view the kernel version and cd to the corresponding file directory
#6.1.21 then run the following command
cd 6.1.21
 
#Step 3: Please check the bits of your system, enter the 32 directory for 32-bit systems, and enter the 64 directory for 64-bit systems
cd 32
#cd 64
 
#Step 4: Enter your corresponding model command to install the driver, pay attention to the selection of the I2C DIP switch
#10.1inch DSI LCD (C) 1280×800 driver:
sudo bash ./WS_xinchDSI_MAIN.sh 101C I2C0
 
#Step 5: Wait for a few seconds, when the driver installation is complete and no error is prompted, restart and load the DSI driver and it can be used normally
sudo reboot
