#!/bin/bash

# Function to update a configuration file by adding or removing lines
# Arguments: 
#   1) operation (add or remove)
#   2) config file path
#   3) associative array of configuration parameters and comments
update_config_file() {
    local operation="$1"
    local file="$2"
    local -n config_params=$3  # Using a nameref for associative array
    local line comment

    # Ensure the config file exists
    if [ ! -f "$file" ]; then
        echo "Error: Configuration file '$file' does not exist."
        exit 1
    fi

    # Check if the file is writable; if not, escalate permissions
    if [ ! -w "$file" ]; then
        echo "Configuration file '$file' is not writable. Attempting to escalate permissions..."
        sudo -v  # Ask for root password
        if [ $? -ne 0 ]; then
            echo "Error: Failed to obtain root permissions. Cannot proceed."
            exit 1
        fi
    fi

    # Check for [all] section existence
    if ! grep -qF "[all]" "$file"; then
        echo "[all] section not found. Adding it to the file."
        echo "[all]" | sudo tee -a "$file" > /dev/null
    fi

    # Loop through configuration parameters and add/remove them
    for line in "${!config_params[@]}"; do
        comment="${config_params[$line]}"
        if [ "$operation" = "add" ]; then
            # Ensure the line is added only under [all]
            if grep -qF "$line" "$file"; then
                if grep -qF "[all]" -A 10 "$file" | grep -qF "$line"; then
                    echo "Configuration '$line' already exists under [all]."
                else
                    echo "Configuration '$line' exists under a different section. Adding it under [all]."
                    echo "# $comment" | sudo tee -a "$file" > /dev/null
                    echo "$line" | sudo tee -a "$file" > /dev/null
                fi
            else
                echo "Adding configuration: $line under [all]."
                echo "# $comment" | sudo tee -a "$file" > /dev/null
                echo "$line" | sudo tee -a "$file" > /dev/null
            fi
        elif [ "$operation" = "remove" ]; then
            if grep -qF -- "$line" "$file"; then
                echo "Removing configuration: $line"
                sudo sed -i "/$line/d" "$file"
            else
                echo "Configuration '$line' not found in $file. No action taken."
            fi
        else
            echo "Error: Unknown operation '$operation'. Supported operations: add, remove."
            exit 1
        fi
    done
}

# USB Communication Configuration Parameters
declare -A usb_config_params=(
    ["dtoverlay=dwc2,dr_mode=host"]="Enable USB communication over Type-C USB port"
)

# Main script for USB Communication Configuration
config_file="/boot/firmware/config.txt"

# Prompt user for action
echo "Do you want to enable USB communication over Type-C USB port? (Y/n)"
read -r enable_choice
enable_choice=$(echo "$enable_choice" | tr '[:upper:]' '[:lower:]')  # Normalize to lowercase

if [ "$enable_choice" = "y" ] || [ -z "$enable_choice" ]; then
    update_config_file "add" "$config_file" usb_config_params
    echo "USB communication enabled. Please reboot your Raspberry Pi."
else
    echo "Do you want to disable USB communication over Type-C USB port? (Y/n)"
    read -r disable_choice
    disable_choice=$(echo "$disable_choice" | tr '[:upper:]' '[:lower:]')  # Normalize to lowercase
    if [ "$disable_choice" = "y" ]; then
        update_config_file "remove" "$config_file" usb_config_params
        echo "USB communication disabled. Please reboot your Raspberry Pi."
    else
        echo "No changes made."
    fi
fi
