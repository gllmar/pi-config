#!/bin/bash

# Function to install dependencies
install_dependencies() {
    echo "Installing dependencies..."
    # Add commands to install dependencies here
    cd "$OF_ROOT"
    cd scripts/linux/debian/
    sudo ./install_dependencies.sh 
    sudo ./install_codecs.sh 
}

compile_OF(){
    echo "Compilling OF..."
    cd "$OF_ROOT"
    cd scripts/linux
    ./compileOF.sh -j3
}


# Function to get CPU architectures
get_architecture() {
    echo "Detecting CPU architecture..."
    arch=$(uname -m)  # Corrected line
    echo "Architecture detected: $arch"
    echo $arch  # Echo the value instead of returning it
}

# Function to clone OpenFrameworks
clone_openframeworks() {
    OF_ROOT=~/src/openframeworks/of-git
    echo "Cloning OpenFrameworks from GitHub..."
    git clone https://github.com/openframeworks/openFrameworks.git "$OF_ROOT"  --depth 1 
    cd ~/src/openframeworks/of-git
    git submodule update --init --recursive
    git submodule update --recursive
    cd $OF_ROOT/scripts/dev/
    ./download_libs.sh 
}

# Function to download and extract a specific version
download_openframeworks() {
    local version=$1
    echo "Downloading OpenFrameworks version: $version..."
    # Add commands to download and extract the specified version
}

update_profile_for_of_root() {
    local profile_file=~/.profile
    echo "OF_ROOT = $OF_ROOT"

    # Check if OF_ROOT is already set in .profile
    if grep -q "export OF_ROOT=" "$profile_file"; then
        echo "OF_ROOT is already set in .profile, updating the value..."
        # Update the OF_ROOT line
        sed -i "s|export OF_ROOT=.*|export OF_ROOT=$OF_ROOT|" "$profile_file"
    else
        echo "Setting OF_ROOT in .profile..."
        echo "export OF_ROOT=$OF_ROOT" >> "$profile_file"
    fi
}

# Function to set OF_ROOT in ~/.bashrc
update_bashrc_for_of_root() {
    local bashrc_file=~/.bashrc

    # Check if OF_ROOT is already set in .bashrc
    if grep -q "export OF_ROOT=" "$bashrc_file"; then
        echo "OF_ROOT is already set in .bashrc, updating the value..."
        # Update the OF_ROOT line
        sed -i "s|export OF_ROOT=.*|export OF_ROOT=$OF_ROOT|" "$bashrc_file"
    else
        echo "Setting OF_ROOT in .bashrc..."
        echo "export OF_ROOT=$OF_ROOT" >> "$bashrc_file"
    fi
}

# Main installation function
main() {
    arch=$(get_architecture)

    echo "Select installation method:"
    echo "1) Compile from source"
    echo "2) Install from nightly build"
    echo "3) Install from release"
    read -p "Enter your choice (1-3): " choice

    case $choice in
        1)
            clone_openframeworks
            ;;
        2)
            echo "Fetching nightly builds..."
            echo "TODO"
            # Add commands to list and select from nightly builds
            ;;
        3)
            echo "Fetching releases..."
            echo "TODO"
            # Add commands to list and select from releases
            ;;
        *)
            echo "Invalid choice"
            exit 1
            ;;
    esac

    update_profile_for_of_root
    update_bashrc_for_of_root
    install_dependencies
    compile_OF

    # Add additional configuration steps if needed
}

# Start the script
main
