```
name="webapp-shellinabox"; script="setup-$name.sh"; curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script && sudo bash $script && rm $script
```

install shellinabox

sudo apt install shellinabox


```
cat /etc/nginx/conf.d/shellinabox_forwarding
```

```
    location ~* ^/(shell|sh|ssh|terminal)/ {
        proxy_pass http://127.0.0.1:4200;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $host;
    }
```
    

restart service 

shellinabox.service

reload nginx
