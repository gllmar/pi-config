#!/bin/bash

# Configuration variables
SHELLINABOX_DEFAULT="/etc/default/shellinabox"
SHELLINABOX_CSS="/etc/shellinabox/options-enabled/00_White On Black.css"
NGINX_CONF="/etc/nginx/conf.d/shellinabox_forwarding"

# Function to install ShellInABox
install_shellinabox() {
    echo "Installing ShellInABox..."
    sudo apt update
    sudo apt install -y shellinabox

    configure_shellinabox
}

# Function to configure ShellInABox
configure_shellinabox() {
    echo "Configuring ShellInABox..."

    QUOTED_CSS_FILE="'$SHELLINABOX_CSS'"

    sudo sed -i '/^SHELLINABOX_ARGS=/ s/--disable-ssl//g' "$SHELLINABOX_DEFAULT"
    sudo sed -i '/^SHELLINABOX_ARGS=/ s/--css [^"]*//g' "$SHELLINABOX_DEFAULT"
    sudo sed -i "s|SHELLINABOX_ARGS=\"\(.*\)\"|SHELLINABOX_ARGS=\"\1 --disable-ssl --css $QUOTED_CSS_FILE\"|" "$SHELLINABOX_DEFAULT"

    echo "Disabled SSL and set custom CSS for ShellInABox."

    configure_nginx_forwarding

    sudo systemctl restart shellinabox.service
}

# Function to configure Nginx for port forwarding
configure_nginx_forwarding() {
    read -p "Do you want to deploy Nginx port forwarding for ShellInABox? (y/n): " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        echo "Configuring Nginx port forwarding..."
        sudo bash -c "cat > $NGINX_CONF" << 'EOF'
location ~* ^/(shell|sh|ssh|terminal)/ {
    proxy_pass http://127.0.0.1:4200;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
}
EOF
        sudo systemctl restart nginx
        echo "Nginx port forwarding configured and service restarted."
    else
        echo "Skipping Nginx port forwarding configuration."
    fi
}

# Function to uninstall ShellInABox
uninstall_shellinabox() {
    echo "Uninstalling ShellInABox..."
    sudo apt remove --purge -y shellinabox
    echo "ShellInABox has been uninstalled."
}

# Ask the user if they want to install ShellInABox
read -p "Do you want to install ShellInABox? (y/n): " -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
    install_shellinabox
else
    read -p "ShellInABox is not installed. Would you like to uninstall it? (y/n): " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        uninstall_shellinabox
    else
        echo "No changes made."
    fi
fi
