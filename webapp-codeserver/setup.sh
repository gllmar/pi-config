#!/bin/bash

# Check if code-server is already installed
if [[ -f /usr/bin/code-server ]]; then
    read -p "code-server is already installed. Do you want to reinstall it? (yes/no) [yes]: " reinstall
    reinstall=${reinstall:-yes}
    if [[ $reinstall == "yes" || $reinstall == "y" || $reinstall == "Y" ]]; then
        echo "Reinstalling code-server..."
        curl -fsSL https://code-server.dev/install.sh | sh
    fi
else
    echo "Installing code-server..."
    curl -fsSL https://code-server.dev/install.sh | sh
fi

# Check if config file exists
config_file="/home/$USER/.config/code-server/config.yaml"
if [[ -f $config_file ]]; then
    echo "Current configuration:"
    cat $config_file
    read -p "Do you want to reconfigure code-server settings? (yes/no) [yes]: " reconfigure
    reconfigure=${reconfigure:-yes}
else
    reconfigure="yes"
fi

if [[ $reconfigure == "yes" || $reconfigure == "y" || $reconfigure == "Y" ]]; then
    # Obtain configuration input from user
    read -p "Bind to localhost only? (default: no) [no]: " bind_localhost
    read -p "Bind port (default: 8070) [8070]: " bind_port
    read -p "Password (default: password) [password]: " password

    # Set default values if user input is empty
    bind_localhost=${bind_localhost:-no}
    bind_port=${bind_port:-8070}
    password=${password:-password}

    # Determine bind address based on user input
    if [[ $bind_localhost == "yes" || $bind_localhost == "Yes" ]]; then
        bind_addr="127.0.0.1:$bind_port"
    else
        bind_addr="0.0.0.0:$bind_port"
    fi

    # Create config directory if it doesn't exist
    mkdir -p /home/$USER/.config/code-server

    # Write configuration to config.yaml
    echo "Writing configuration to config.yaml..."
    cat <<EOL > $config_file
bind-addr: $bind_addr
auth: password
password: $password
cert: false
EOL
fi

read -p "Do you want to enable and restart code-server service? (yes/no) [yes]: " service_action
service_action=${service_action:-yes}

if [[ $service_action == "yes" || $service_action == "y" || $service_action == "Y" ]]; then
    echo "Enabling and restarting code-server service..."
    sudo systemctl enable --now code-server@$USER
    sudo systemctl restart --now code-server@$USER
else
    echo "You can enable and restart code-server service later with the following commands:"
    echo "sudo systemctl enable --now code-server@$USER"
    echo "sudo systemctl restart --now code-server@$USER"
fi

# Function to configure NGINX for code forwarding
configure_nginx() {
    # Extract the port from code-server config.yaml
    bind_port=$(grep 'bind-addr:' /home/$USER/.config/code-server/config.yaml | awk -F ':' '{print $3}')

    # If bind_port is empty, exit the function
    if [[ -z $bind_port ]]; then
        echo "Failed to retrieve the bind port from config.yaml. Please check the configuration."
        return 1
    fi

    # Get the hostname of the machine
    server_name=$(hostname)

    # Create an NGINX configuration file for code forwarding
    nginx_config_file="/etc/nginx/conf.d/code_forwarding"
    echo "Creating NGINX configuration for code forwarding..."
    cat <<EOL | sudo tee $nginx_config_file

    location ~ ^/code(/.*)?$ {
        # Use return for a straightforward redirect
        return 302 http://\$host:$bind_port\$1;
    }

EOL

    # Test NGINX configuration and reload if successful
    if sudo nginx -t; then
        echo "Reloading NGINX..."
        sudo systemctl reload nginx
    else
        echo "Failed to configure NGINX. Please check the configuration."
    fi
}

read -p "Do you want to configure NGINX for code forwarding? (yes/no) [yes]: " configure_nginx_answer
configure_nginx_answer=${configure_nginx_answer:-yes}

if [[ $configure_nginx_answer == "yes" || $configure_nginx_answer == "y" || $configure_nginx_answer == "Y" ]]; then
    configure_nginx
fi

echo "Setup complete."
