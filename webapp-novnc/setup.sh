#!/bin/bash

echo "⚡ Setting up noVNC and WayVNC on Raspberry Pi OS ⚡"
echo ""

# Update system
echo "🔄 Updating package lists..."
sudo apt-get update && sudo apt-get upgrade -y

# Install required packages
echo "📦 Installing noVNC and Nginx..."
sudo apt-get install -y novnc nginx

# Check if WayVNC is enabled in raspi-config
WAYVNC_ENABLED=$(sudo raspi-config nonint get_vnc)
if [[ "$WAYVNC_ENABLED" != "0" ]]; then
    echo "❌ WayVNC is not enabled."
    read -p "❓ Enable WayVNC now? (Y/n): " ENABLE_WAYVNC
    ENABLE_WAYVNC=${ENABLE_WAYVNC:-Y}

    if [[ "$ENABLE_WAYVNC" =~ ^[Yy]$ ]]; then
        echo "🔄 Enabling WayVNC in raspi-config..."
        sudo raspi-config nonint do_vnc 0
        echo "✅ WayVNC has been enabled. Reboot may be required."
    else
        echo "❌ WayVNC is required for noVNC to function. Exiting."
        exit 1
    fi
else
    echo "✅ WayVNC is already enabled."
fi

# Check if the user wants to disable authentication (default: Yes)
echo ""
read -p "❓ Disable authentication and TLS in WayVNC for noVNC compatibility? (Y/n): " DISABLE_AUTH
DISABLE_AUTH=${DISABLE_AUTH:-Y}

if [[ "$DISABLE_AUTH" =~ ^[Yy]$ ]]; then
    echo "⚙️  Modifying WayVNC configuration..."
    sudo sed -i 's/^enable_auth=.*/enable_auth=false/' /etc/wayvnc/config
    sudo sed -i 's/^enable_pam=.*/enable_pam=false/' /etc/wayvnc/config
    sudo sed -i 's/^private_key_file=.*/#private_key_file=tls_key.pem/' /etc/wayvnc/config
    sudo sed -i 's/^certificate_file=.*/#certificate_file=tls_cert.pem/' /etc/wayvnc/config
    sudo sed -i 's/^rsa_private_key_file=.*/#rsa_private_key_file=rsa_key.pem/' /etc/wayvnc/config
    sudo sed -i 's/^address=.*/address=0.0.0.0/' /etc/wayvnc/config
    echo "✅ WayVNC authentication and encryption disabled for compatibility."
fi

# Restart WayVNC
echo "🔄 Restarting WayVNC..."
sudo systemctl restart wayvnc
sleep 2

# Verify that WayVNC is running on port 5900
if ss -tulnp | grep -q ":5900"; then
    echo "✅ WayVNC is running on port 5900."
else
    echo "❌ ERROR: WayVNC is not running correctly. Check logs with: journalctl -u wayvnc"
    exit 1
fi

# Create noVNC systemd service
echo "⚙️  Creating noVNC service..."
NOVNC_SERVICE_FILE="/etc/systemd/system/novnc.service"
sudo bash -c "cat > $NOVNC_SERVICE_FILE" <<EOL
[Unit]
Description=noVNC WebSocket proxy
After=wayvnc.service

[Service]
ExecStart=/usr/share/novnc/utils/novnc_proxy --vnc localhost:5900 --listen 6080
Restart=on-failure
User=pi

[Install]
WantedBy=multi-user.target
EOL

# Reload systemd and enable the service
echo "🔄 Enabling and starting noVNC service..."
sudo systemctl daemon-reload
sudo systemctl enable novnc
sudo systemctl restart novnc
sleep 2

# Check if noVNC is running on port 6080
if ss -tulnp | grep -q ":6080"; then
    echo "✅ noVNC is running on port 6080."
else
    echo "❌ ERROR: noVNC is not running correctly. Check logs with: journalctl -u novnc"
    exit 1
fi

# Ask if user wants to enable Nginx forwarding
echo ""
read -p "❓ Enable Nginx forwarding for noVNC (access via http://<pi-ip>/novnc/)? (Y/n): " ENABLE_NGINX
ENABLE_NGINX=${ENABLE_NGINX:-Y}

if [[ "$ENABLE_NGINX" =~ ^[Yy]$ ]]; then
    echo "⚙️  Configuring Nginx for noVNC..."
    NOVNC_NGINX_CONF="/etc/nginx/conf.d/novnc"
    sudo bash -c "cat > $NOVNC_NGINX_CONF" <<EOL

  # Serve NoVNC at /novnc/
  location /novnc/ {
      alias /usr/share/novnc/;
      index vnc.html;
      try_files \$uri \$uri/ =404;
  }

  # WebSocket Proxy for noVNC
  location /websockify {
      proxy_pass http://127.0.0.1:6080;
      proxy_http_version 1.1;
      proxy_set_header Upgrade \$http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_set_header Host \$host;
      proxy_read_timeout 60;
      proxy_send_timeout 60;
  }

EOL

    # Restart Nginx
    echo "🔄 Restarting Nginx..."
    sudo systemctl restart nginx

    echo "✅ Nginx forwarding enabled. Access noVNC at:"
    echo "🌐 http://$(hostname -I | awk '{print $1}')/novnc/"
fi

echo ""
echo "🎉 Setup Complete! You can now access noVNC at:"
echo "🌐 http://$(hostname -I | awk '{print $1}'):6080/vnc.html"
echo "🌐 (Or via Nginx if enabled) http://$(hostname -I | awk '{print $1}')/novnc/"
echo ""
echo "Press 'Connect' to access your Raspberry Pi desktop remotely."
echo "🚀 Enjoy your noVNC + WayVNC setup!"
