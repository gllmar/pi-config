# novnc
webfacing vnc client

```
name="webapp-novnc"; script="setup-$name.sh"; curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script && bash $script && rm $script
```

## need to build wayvnc
until wayvnc is shipped with recent implementations, https://github.com/raspberrypi/bookworm-feedback/issues/5


### dependencies



#### build novnc-dep

```
sudo apt install meson libdrm-dev libxkbcommon-dev libwlroots-dev libjansson-dev libpam0g-dev libgnutls28-dev libavfilter-dev libavcodec-dev  libavutil-dev libturbojpeg0-dev scdoc

mkdir -p ~/src/novnc-dep
cd  ~/src/novnc-dep

git clone https://github.com/any1/wayvnc.git
git clone https://github.com/any1/neatvnc.git
git clone https://github.com/any1/aml.git

mkdir wayvnc/subprojects
cd wayvnc/subprojects
ln -s ../../neatvnc .
ln -s ../../aml .
cd -

mkdir neatvnc/subprojects
cd neatvnc/subprojects
ln -s ../../aml .
cd -
cd wayvnc
meson build
ninja -C build
sudo ninja -C build install
sudo ldconfig
```


####  
```
cat /etc/xdg/autostart/wayvnc.desktop
```

```
[Desktop Entry]
Type=Application
Name=wayvnc
Comment=Start wayvnc
NoDisplay=true
Exec=/usr/bin/wayvnc --render-cursor --websocket 127.0.0.1
OnlyShowIn=wayfire
```

```
cat ~/.config/wayvnc/config
```
```
address=127.0.0.1
enable_auth=false
```
```
cat /etc/nginx/conf.d/novnc-forwarding
```

```
# Serve NoVNC at URL
location /novnc/ {
    alias /usr/share/novnc/;
    index vnc_auto.html;
    try_files $uri $uri/ =404;
}
    
location /websockify {
    proxy_pass http://127.0.0.1:5900;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
}
```

permission

Add www-data to shadow group (so that nginx webserver can read /etc/shadow for PAM authentication), and restart

sudo usermod -aG shadow www-data

ask to reboot 

sudo reboot