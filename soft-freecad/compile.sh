#!/bin/bash



# Update package lists
sudo apt-get update

# Install dependencies
sudo apt-get install -y build-essential cmake git libboost-all-dev \
libcoin-dev libeigen3-dev libgmp-dev libkdtree++-dev libmedc-dev libocct-data-exchange-dev \
libocct-ocaf-dev libocct-foundation-dev libocct-modeling-data-dev libocct-modeling-algorithms-dev \
libocct-visualization-dev libpyside2-dev qtbase5-dev libqt5svg5-dev libqt5xmlpatterns5-dev \
qttools5-dev libqt5opengl5-dev libqt5webkit5-dev libqt5webenginewidgets5-dev \
libqt5x11extras5-dev libvtk7-dev libxerces-c-dev libzipios++-dev libshiboken2-dev \
python3-dev python3-matplotlib python3-pivy python3-ply python3-pyside2.qtsvg \
python3-pyside2.qtuitools swig

# Clone FreeCAD source
cd ~/src
git clone --recurse-submodules https://github.com/FreeCAD/FreeCAD.git FreeCAD_source
cd FreeCAD_source

# Create build directory
mkdir build
cd build

# Run CMake to configure the build
cmake ../freecad-source -DBUILD_QT5=ON -DPYTHON_EXECUTABLE=/usr/bin/python3 -DUSE_PYBIND11=ON

# Compile
make -j$(nproc)

# Install (optional)
sudo make install

echo "FreeCAD installation completed."
