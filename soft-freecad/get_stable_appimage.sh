#!/bin/bash

# Manually set this to one of the available versions you wish to download
version="0.21.2" # Example version; replace with the actual version you're interested in

# Function to get the AppImage download URL for the specified version
get_appimage_url() {
    local version="$1"
    echo "Fetching the AppImage download URL for version $version..."
    local download_url=$(curl -s "https://api.github.com/repos/FreeCAD/FreeCAD/releases/tags/$version" | \
        jq -r '.assets[] | select(.name | test("Linux-aarch64.AppImage$")) | .browser_download_url' | head -n 1)
    
    if [[ -z "$download_url" ]]; then
        echo "Failed to find the AppImage for version $version."
    else
        echo "Download URL: $download_url"
    fi
}

get_appimage_url "$version"
