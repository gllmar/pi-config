#!/bin/bash

# Default installation directory
DEFAULT_INSTALL_DIR="$HOME/Applications"

# Ensure jq is installed
if ! command -v jq &> /dev/null; then
    echo "jq is not installed. Please install jq to run this script."
    exit 1
fi

# Function to get the latest weekly build URL
get_latest_weekly_build_url() {
    echo "Fetching the latest weekly build..."
    local latest_url=$(curl -s "https://api.github.com/repos/FreeCAD/FreeCAD-Bundle/releases" | \
        jq -r '.[] | select(.tag_name == "weekly-builds") | .assets[] | select(.name | endswith("Linux-aarch64-py311.AppImage")) | .browser_download_url' | head -1)
    if [[ -z "$latest_url" ]]; then
        echo "Failed to fetch the latest weekly build URL."
        exit 1
    fi
    echo "$latest_url"
}

# Function to list and select a stable version
select_stable_version() {
    echo "Fetching available FreeCAD versions..."
    local versions=$(curl -s "https://api.github.com/repos/FreeCAD/FreeCAD/releases" | jq -r '.[].tag_name' | grep -v "weekly" | sort -Vr)
    if [[ -z "$versions" || "$versions" == "null" ]]; then
        echo "Failed to fetch versions. Please check your connection or GitHub API rate limits."
        exit 1
    fi

    local versions_array=($versions)
    echo "Available versions:"
    select version_choice in "${versions_array[@]}" "Latest"; do
        [[ -n "$version_choice" ]] && break
        echo "Invalid choice, please try again."
    done

    version_choice=${version_choice:-Latest}
    if [[ "$version_choice" == "Latest" ]]; then
        version_choice=${versions_array[0]}
    fi
    echo "Selected version: $version_choice"
    echo "$version_choice"
}

# Function to install FreeCAD
install_freecad() {
    local version="$1"
    local install_dir="$2"
    local bleeding_edge="$3"

    local download_url
    if [ "$bleeding_edge" = "yes" ]; then
        download_url=$(get_latest_weekly_build_url)
    else
        echo "Fetching the release for version $version..."
        local assets=$(curl -s "https://api.github.com/repos/FreeCAD/FreeCAD/releases/tags/$version" | jq -r '.assets[].browser_download_url')
        download_url=$(echo "$assets" | grep "Linux-aarch64.AppImage$" | head -n 1)
        if [[ -z "$download_url" ]]; then
            echo "No compatible FreeCAD AppImage found for version $version."
            exit 1
        fi
    fi

    echo "Download URL: $download_url"
    mkdir -p "$install_dir"
    local appimage_file="$install_dir/FreeCAD-$(basename "$download_url")"
    echo "Downloading FreeCAD..."
    wget --show-progress "$download_url" -O "$appimage_file" && chmod +x "$appimage_file" || { echo "Download failed."; exit 1; }

    echo "FreeCAD has been installed successfully."
}

# Main installation function
main() {
    echo "Do you want to use the bleeding edge weekly build? (Y/n):"
    read -r use_bleeding_edge
    local version="latest"
    if [[ $use_bleeding_edge =~ ^[Yy](es)?$ ]]; then
        use_bleeding_edge="yes"
    else
        use_bleeding_edge="no"
        version=$(select_stable_version)
    fi

    echo "Enter the installation directory or leave blank for default ($DEFAULT_INSTALL_DIR):"
    read -r install_dir
    install_dir=${install_dir:-"$DEFAULT_INSTALL_DIR"}

    install_freecad "$version" "$install_dir" "$use_bleeding_edge"
}

main
