#!/bin/bash

# Function to list available FreeCAD versions
list_versions() {
    echo "Fetching available FreeCAD versions..."
    local versions=$(curl -s "https://api.github.com/repos/FreeCAD/FreeCAD/releases" | jq -r '.[].tag_name' | grep -v "weekly" | sort -Vr)
    echo "Available versions:"
    echo "$versions"
}

list_versions
