#!/bin/bash

# Ensure dependencies are installed
dependencies=(cvlc nginx dialog)
missing_dependencies=()

echo "Checking for required dependencies..."
for dep in "${dependencies[@]}"; do
    if ! command -v "$dep" &> /dev/null; then
        missing_dependencies+=("$dep")
    fi
done

if [ "${#missing_dependencies[@]}" -gt 0 ]; then
    echo "Missing dependencies: ${missing_dependencies[*]}"
    echo "Installing missing dependencies..."
    sudo apt-get update
    sudo apt-get install -y "${missing_dependencies[@]}"
fi

# Prompt the user for service installation
read -p "Do you want to install the VLC Loop User Service? (y/n) " install_service
if [[ "$install_service" != "y" ]]; then
    echo "Service installation canceled."
    exit 0
fi

# Create configuration directory if it doesn't exist
config_dir="$HOME/.config/cvlc_loop"
mkdir -p "$config_dir"

# Environment file for VLC service
env_file="$config_dir/vlc-loop.env"

# Prompt for media file selection
selected_file=$(dialog --title "Select Media File to Loop" --fselect "$HOME/Videos/" 10 60 3>&1 1>&2 2>&3)
exit_status=$?
clear
if [[ $exit_status -eq 0 && -n "$selected_file" ]]; then
    echo "Selected media: $selected_file"
    echo "MEDIA_PATH=\"$selected_file\"" > "$env_file"
else
    echo "Media selection canceled."
    exit 1
fi

# Validate the selected media file
if [[ ! -f "$selected_file" ]]; then
    echo "Error: Selected file does not exist: $selected_file"
    exit 1
fi

# Prompt for VLC HTTP password
read -sp "Enter a password for VLC HTTP interface: " http_password
echo

# Define the VLC control script
control_script="$config_dir/vlc_control.sh"
cat <<EOF > "$control_script"
#!/bin/bash

# Configuration
ENV_FILE="$env_file"

# Function to update media file
update_media() {
    local new_media="\$1"
    if [[ -z "\$new_media" ]]; then
        echo "Usage: \$0 <new_media_file>"
        exit 1
    fi

    if [[ ! -f "\$new_media" ]]; then
        echo "Error: File does not exist: \$new_media"
        exit 1
    fi

    echo "Updating media file to: \$new_media"
    echo "MEDIA_PATH=\"\$new_media\"" > "\$ENV_FILE"
    systemctl --user restart vlc-loop.service
    echo "VLC Loop Service restarted with new media."
}

# Check if a new media file is provided
if [[ "\$#" -gt 0 ]]; then
    update_media "\$1"
else
    echo "Usage: \$0 <new_media_file>"
    echo "Example: \$0 /path/to/new_media.mp4"
fi
EOF

chmod +x "$control_script"
echo "VLC control script created at: $control_script"

# Define the service file content
service_file="$HOME/.config/systemd/user/vlc-loop.service"
cat <<EOF > "$service_file"
[Unit]
Description=VLC Loop Service
After=wait-for-desktop-loaded.service

[Service]
EnvironmentFile=$env_file
Environment=DISPLAY=:0
ExecStart=cvlc "\$MEDIA_PATH" --input-repeat=999999 --fullscreen --no-osd --extraintf http --http-password $http_password --http-port 9990
Restart=always
RestartSec=5

[Install]
WantedBy=default.target
EOF

# Reload systemd manager configuration
systemctl --user daemon-reload
echo "Service file created and systemd reloaded."

# Start and enable the service
read -p "Do you want to enable and start the VLC Loop Service now? (y/n) " enable_service
if [[ "$enable_service" == "y" ]]; then
    systemctl --user enable vlc-loop.service
    systemctl --user start vlc-loop.service
    echo "VLC Loop Service enabled and started."
fi

# Ask the user if they want to add a redirection in nginx configuration
read -p "Do you want to add a redirection in nginx conf.d for VLC Loop? (y/n) " nginx_choice
if [[ $nginx_choice == "y" || $nginx_choice == "Y" ]]; then
    # Create a temporary file with the specified content
    echo -e "location ~ ^/vlc-folder(/.*)?$ {\n\t# Use return for a straightforward redirect\n\treturn 302 http://\$host:9990\$1;\n}" > /tmp/vlcloop_forward
    
    # Move the temporary file to the nginx conf.d directory with sudo privileges
    sudo mv /tmp/vlcloop_forward /etc/nginx/conf.d/vlcloop_forward
    echo "Redirection added to nginx conf.d as vlcloop_forward"

    # Reload nginx
    echo "Checking nginx config..."
    sudo nginx -t
    echo "Reloading nginx..."
    sudo systemctl restart nginx
    echo "Nginx reloaded with new configuration."
fi
