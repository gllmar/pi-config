#!/bin/bash

# Define the path to the SSH key
KEY_PATH="$HOME/.ssh/id_ed25519"

# Get the name of the computer
COMPUTER_NAME=$(hostname)

# Get the current date formatted as _YYYYMMDD
CURRENT_DATE=$(date +_%Y%m%d)

# Construct the comment
COMMENT="$COMPUTER_NAME$CURRENT_DATE"

# Check if the SSH key already exists
if [[ -f $KEY_PATH ]]; then
    echo "Key already exists. Here's your public key:"
else
    # Generate a new SSH key pair with the specified comment
    ssh-keygen -t ed25519 -C "$COMMENT" -f $KEY_PATH -N ""
    echo "SSH key pair generated. Here's your public key:"
fi

# Output the public key
cat $KEY_PATH.pub
