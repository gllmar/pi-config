#!/bin/bash

# Associative array for environment variables
declare -A env_vars=(
    ["XDG_SESSION_ID"]="\$(id -u)"
    ["XDG_RUNTIME_DIR"]="\"/run/user/\$(id -u)\""
    ["DBUS_SESSION_BUS_ADDRESS"]="\"unix:path=/run/user/\$(id -u)/bus\""
)

# Path to .bashrc
BASHRC="$HOME/.bashrc"

# Function to check if variables are already deployed
check_deployment() {
    local FUNC_NAME="custom_export_env_vars"
    grep -Fq "$FUNC_NAME" "$BASHRC"
}

# Function to manage environment variables in .bashrc
manage_env_vars() {
    local FUNC_NAME="custom_export_env_vars"
    local FUNC_CONTENT="custom_export_env_vars() {\n"
    
    for var in "${!env_vars[@]}"; do
        FUNC_CONTENT+="    [ -z \"\$$var\" ] && export $var=${env_vars[$var]}\n"
    done
    FUNC_CONTENT+="}\ncustom_export_env_vars\n"

    case $1 in
        install)
            if check_deployment; then
                echo "Variables already installed. Skipping installation."
            else
                echo -e "\n$FUNC_CONTENT" >> "$BASHRC"
                echo "Variables installed."
            fi
            ;;
        reinstall)
            sed -i "/$FUNC_NAME/,+$((${#env_vars[@]} + 3))d" "$BASHRC"
            echo -e "\n$FUNC_CONTENT" >> "$BASHRC"
            echo "Variables reinstalled."
            ;;
        remove)
            sed -i "/$FUNC_NAME/,+$((${#env_vars[@]} + 3))d" "$BASHRC"
            echo "Variables removed."
            ;;
        *)
            echo "Invalid choice. Choose install, reinstall, or remove."
            ;;
    esac
}

# Check if variables are already installed
if check_deployment; then
    echo "Environment variables are already deployed."
    read -p "Do you want to reinstall or remove them? (reinstall/remove/skip): " action
    case $action in
        reinstall)
            manage_env_vars reinstall
            ;;
        remove)
            manage_env_vars remove
            ;;
        skip)
            echo "Skipping deployment."
            ;;
        *)
            echo "Invalid choice. Exiting."
            ;;
    esac
else
    echo "Environment variables are not deployed. Proceeding with installation."
    manage_env_vars install
fi
