#!/bin/bash

# Update and upgrade the system
echo "Updating and upgrading the system..."
sudo apt-get update && sudo apt-get upgrade -y

# Install common dependencies
echo "Installing common build tools and development libraries..."
sudo apt-get install -y build-essential checkinstall cmake git libmbedtls-dev libasound2-dev \
libavcodec-dev libavdevice-dev libavfilter-dev libavformat-dev libavutil-dev \
libcurl4-openssl-dev libfontconfig1-dev libfreetype6-dev libgl1-mesa-dev \
libjack-jackd2-dev libjansson-dev libluajit-5.1-dev libpulse-dev libqt5x11extras5-dev \
libspeexdsp-dev libswresample-dev libswscale-dev libudev-dev libv4l-dev libvlc-dev \
libx11-dev libx11-xcb-dev libx264-dev libxcb-randr0-dev libxcb-shm0-dev libxcb-xinerama0-dev \
libxcomposite-dev libxinerama-dev pkg-config python3-dev qtbase5-dev libqt5svg5-dev swig \
libwayland-dev libpipewire-0.3-dev libsystemd-dev ffmpeg libwebsocketpp-dev libasio-dev \
nlohmann-json3-dev libdrm-dev

# Install mesa 
sudo apt-get install libgles2-mesa-dev

# Install Ninja build system and GCC/G++
echo "Installing Ninja build system and GCC/G++ compilers..."
sudo apt-get install -y ninja-build gcc g++

# Install XCB libraries for OBS Studio
echo "Installing additional XCB libraries..."
sudo apt-get install -y libxcb-xfixes0-dev libxcb-composite0-dev

# Install Qt6 dependencies
echo "Installing Qt6 dependencies..."
sudo apt install -y qt6-base-dev qt6-base-dev-tools qt6-base-private-dev qt6-svg-dev libqrcodegencpp-dev librist-dev libpci-dev libva-dev libva-drm2

# Install SRT
sudo apt-get install -y tclsh pkg-config cmake libssl-dev build-essential
echo "Installing SRT..."
if [ ! -d "srt" ]; then
    git clone https://github.com/Haivision/srt.git
fi
cd srt
# Check if configure script exists
if [ -f "./configure" ]; then
    ./configure
    make -j3
    sudo make install
else
    echo "Configure script for SRT not found."
    exit 1
fi
cd ..


# Option to install CEF (Chromium Embedded Framework) for browser support
read -p "Do you want to install browser support (CEF)? [y/N]: " install_browser
if [[ $install_browser == [yY] || $install_browser == [yY][eE][sS] ]]; then
    # Define CEF version and download URL for ARM64
    CEF_VERSION="119.4.7+g55e15c8+chromium-119.0.6045.199"
    CEF_BINARY_URL="https://cef-builds.spotifycdn.com/cef_binary_${CEF_VERSION}_linuxarm64.tar.bz2"
    CEF_BINARY_DIR="cef_binary_${CEF_VERSION}_linuxarm64"
    CEF_BINARY_ARCHIVE="cef_binary_${CEF_VERSION}_linuxarm64.tar.bz2"

    # Set CEF_ROOT_DIR
    CEF_ROOT_DIR="$(pwd)/${CEF_BINARY_DIR}/Release"
    export CEF_ROOT_DIR
    echo "CEF_ROOT_DIR is set to: $CEF_ROOT_DIR"

    # Check if CEF directory exists, download and extract if it doesn't
    if [ ! -d "$CEF_BINARY_DIR" ]; then
        echo "Downloading and setting up CEF for ARM64..."
        wget -N $CEF_BINARY_URL
        tar -xjf $CEF_BINARY_ARCHIVE
    else
        echo "CEF directory already exists, skipping download and extraction."
    fi
fi


# Clone and setup OBS Studio
echo "Cloning OBS Studio..."
if [ ! -d "obs-studio" ]; then
    git clone --recursive https://github.com/obsproject/obs-studio.git
fi
cd obs-studio

# If a previous build directory exists, remove it to avoid configuration conflicts
if [ -d "build" ]; then
    echo "Removing previous build directory..."
    rm -rf build
fi

mkdir -p build
cd build

# Configure OBS Studio Build with Qt6
echo "Configuring OBS Studio Build with Qt6..."
cmake -S .. -B . \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_C_FLAGS="-march=native" \
    -DCMAKE_CXX_FLAGS="-march=native" \
    -DENABLE_PIPEWIRE=ON \
    -DENABLE_AJA=OFF \
    -DENABLE_BROWSER=$( [[ $install_browser == [yY] || $install_browser == [yY][eE][sS] ]] && echo "ON" || echo "OFF" ) \
    -DENABLE_QSV11=OFF \
    -DENABLE_WEBRTC=OFF \
    -DOpenGL_GL_PREFERENCE=GLVND

# Determine the number of CPU cores
CPU_CORES=$(nproc)

# Use all but one core for the build process
echo "Building OBS Studio using $(($CPU_CORES - 1)) cores..."
cmake --build . --parallel $(($CPU_CORES - 1))


# Install OBS Studio
echo "Installing OBS Studio..."
sudo make install


# Modify OBS Studio .desktop file to include Mesa override
DESKTOP_FILE="/usr/local/share/applications/com.obsproject.Studio.desktop"
if [ -f "$DESKTOP_FILE" ]; then
    sudo sed -i 's/^Exec=obs/Exec=env MESA_GL_VERSION_OVERRIDE=3.3 obs/' "$DESKTOP_FILE"
fi


# Add instructions for running OBS with MESA override if needed
echo "To run OBS Studio with MESA override, use: MESA_GL_VERSION_OVERRIDE=3.3 obs"
