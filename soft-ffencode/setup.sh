#!/bin/bash

# Define encoding functions
encode_h264() {
    ffmpeg -i "$1" -c:v libx264 -profile:v high -level 4.2 -b:v 10M -g 50 -c:a aac -b:a 192k -f mp4 "${1%.*}_h264.mp4"
}

encode_h265() {
    ffmpeg -i "$1" -c:v libx265 -profile:v main -level 4.1 -b:v 5M -g 50 -c:a aac -b:a 192k -f mp4 "${1%.*}_h265.mp4"
}

# Associative array for presets with function names as keys
declare -A presets
presets=(
    ["encode_h264"]="H.264 (High Quality 1080p)"
    ["encode_h265"]="H.265 (High Efficiency 1080p)"
)

# Function to display help/user manual
display_help() {
    echo "Usage: $0 [video file path] [preset function name]"
    echo "This script encodes a video file into different formats based on the selected preset."
    echo "Options:"
    for key in "${!presets[@]}"; do
        echo "  $key: ${presets[$key]}"
    done
    echo "Example: $0 video.mp4 encode_h264"
}

# Function to install the script
install_script() {
    local script_name=$(basename "$0")
    local install_path="/usr/local/bin/$script_name"

    if [[ -f "$install_path" ]] && ! diff -q "$0" "$install_path" > /dev/null; then
        echo "An older version of ffencode is already installed."
        read -p "Do you want to update it? (y/n) " update_choice
        if [[ $update_choice =~ ^[Nn]$ ]]; then
            echo "Update aborted."
            return
        fi
    fi

    if ! cp "$0" "$install_path" 2>/dev/null; then
        echo "Attempting to install ffencode with elevated privileges..."
        if sudo cp "$0" "$install_path"; then
            echo "ffencode installed successfully to $install_path"
        else
            echo "Failed to install ffencode."
        fi
    else
        echo "ffencode installed successfully to $install_path"
    fi
}

# Function to check for ffmpeg installation and install if not present
check_ffmpeg() {
    if ! command -v ffmpeg &> /dev/null; then
        echo "ffmpeg is not installed."
        local os_name=$(uname)
        if [[ "$os_name" == "Linux" ]]; then
            echo "Attempting to install ffmpeg on Linux..."
            sudo apt-get update && sudo apt-get install -y ffmpeg
        elif [[ "$os_name" == "Darwin" ]]; then
            echo "Attempting to install ffmpeg on macOS..."
            # Check if Homebrew is installed
            if ! command -v brew &> /dev/null; then
                echo "Homebrew not found. Installing Homebrew first..."
                /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
            fi
            brew install ffmpeg
        else
            echo "Unsupported operating system."
            exit 1
        fi
        echo "ffmpeg installed successfully."
    fi
}

# Function to select encoding preset and encode
select_preset_and_encode() {
    echo "Select the encoding preset:"
    local i=1
    for key in "${!presets[@]}"; do
        echo "  $i: ${presets[$key]}"
        let i++
    done

    local choice
    while true; do
        read -p "Enter your choice [1-${#presets[@]}]: " choice
        if [[ $choice =~ ^[0-9]+$ ]] && [ $choice -ge 1 ] && [ $choice -le ${#presets[@]} ]; then
            break
        else
            echo "Invalid choice. Please enter a number between 1 and ${#presets[@]}."
        fi
    done

    local function_names=("${!presets[@]}")
    local selected_function=${function_names[choice-1]}
    if [[ -n $selected_function ]]; then
        $selected_function "$1"
        echo "Encoding completed."
    else
        echo "Error: Invalid preset choice."
        display_help
        exit 1
    fi
}

# Main script logic
check_ffmpeg

if [ $# -eq 0 ]; then
    echo "No arguments provided."
    read -p "Do you want to install ffencode in /usr/local/bin? (y/n) " install_choice
    if [[ $install_choice =~ ^[Yy]$ ]]; then
        install_script
    else
        display_help
    fi
    exit 0
fi

if [ ! -f "$1" ]; then
    echo "Error: File not found."
    display_help
    exit 1
fi

video_file="$1"
preset_function="${2:-}"

if [ -z "$preset_function" ]; then
    select_preset_and_encode "$video_file"
else
    if [[ -n ${presets[$preset_function]} ]]; then
        $preset_function "$video_file"
        echo "Encoding completed."
    else
        echo "Error: Invalid preset choice."
        display_help
        exit 1
    fi
fi
