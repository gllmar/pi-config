#!/bin/bash

# Exit on any error
set -e

# Update package list and install dependencies
echo "Installing required packages..."
sudo apt-get update
sudo apt-get install -y \
    autoconf libtool libasound2-dev build-essential autoconf automake libtool gettext git \
    libjack-jackd2-dev libfftw3-dev tcl tk

# Set up source directory
SRC_DIR="$HOME/src"
PD_REPO="https://github.com/pure-data/pure-data"

# Create source directory if it doesn't exist
if [ ! -d "$SRC_DIR" ]; then
    echo "Creating source directory at $SRC_DIR..."
    mkdir -p "$SRC_DIR"
fi

# Navigate to source directory
cd "$SRC_DIR"

# Clone Pure Data repository
if [ ! -d "pure-data" ]; then
    echo "Cloning Pure Data repository..."
    git clone "$PD_REPO"
else
    echo "Pure Data repository already cloned. Pulling latest changes..."
    cd pure-data
    git pull
    cd ..
fi

# Navigate to Pure Data directory
cd pure-data

# Run build commands
echo "Running build steps for Pure Data..."
./autogen.sh
./configure --enable-jack --enable-fftw
make -j$(nproc)

# Install Pure Data
echo "Installing Pure Data..."
sudo make install

echo "Pure Data has been successfully installed!"
