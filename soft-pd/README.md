```sh
name="soft-pd"; script="setup-$name.sh"; curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script &&  bash $script && rm $script
```

sudo apt-get install autoconf libtool libasound2-dev build-essential autoconf automake libtool gettext git libasound2-dev libjack-jackd2-dev libfftw3-3 libfftw3-dev tcl tk

cd ~/src
git clone https://github.com/pure-data/pure-data
cd pure-data
./autogen.sh
./configure --enable-jack --enable-fftw
make -j4
sudo make install
