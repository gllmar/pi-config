#!/bin/bash

# Function to install dependencies
install_dependencies() {
    echo "Installing dependencies..."
    sudo apt-get install -y interception-tools interception-tools-compat
    sudo apt-get install -y cmake
    cd ~
    git clone https://gitlab.com/interception/linux/plugins/hideaway.git
    cd hideaway
    cmake -B build -DCMAKE_BUILD_TYPE=Release
    cmake --build build
    sudo cp /home/$USER/hideaway/build/hideaway /usr/bin
    sudo chmod +x /usr/bin/hideaway
    echo "Dependencies installed."
}

# Function to configure the tool
configure_tool() {
    echo "Configuring tool..."
    cd ~
    wget https://raw.githubusercontent.com/ugotapi/wayland-pagepi/main/config.yaml
    sudo cp /home/$USER/config.yaml /etc/interception/udevmon.d/config.yaml
    echo "Configuration complete."
}

# Function to restart the service
restart_service() {
    echo "Restarting udevmon service..."
    sudo systemctl restart udevmon
    echo "Service restarted."
}

# Function to enable the tool
enable_tool() {
    install_dependencies
    configure_tool
    restart_service
    echo "Mouse hideaway enabled."
}

# Function to disable the tool
disable_tool() {
    echo "Disabling mouse hideaway..."
    sudo systemctl stop udevmon
    echo "Mouse hideaway disabled."
}

# Prompt for user input if no argument is given
if [ -z "$1" ]; then
    echo "Do you want to enable mouse hideaway? [y/n]"
    read -r action
else
    action=$1
fi

# Main script logic
case "$action" in
    y|e|enable)
        enable_tool
        ;;
    n|d|disable)
        disable_tool
        ;;
    *)
        echo "Invalid input. Please use 'enable' or 'disable'."
        exit 1
        ;;
esac
