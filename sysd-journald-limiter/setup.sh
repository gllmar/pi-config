#!/bin/bash

# Function to run command with sudo if not already root
run_as_root() {
    if [ "$(id -u)" -ne 0 ]; then
        sudo "$@"
    else
        "$@"
    fi
}

# Step 1: Backup the original journald.conf file
run_as_root cp /etc/systemd/journald.conf /etc/systemd/journald.conf.backup

# Step 2: Update journald.conf with new limits
run_as_root sed -i '/^#SystemMaxUse=/c\SystemMaxUse=10M' /etc/systemd/journald.conf
run_as_root sed -i '/^#RuntimeMaxUse=/c\RuntimeMaxUse=10M' /etc/systemd/journald.conf

# Step 3: Clear journal logs
run_as_root journalctl --vacuum-size=10M

# Step 4: Restart systemd-journald
run_as_root systemctl restart systemd-journald

echo "Journal size limit set to 10M, logs cleared, and systemd-journald restarted."
