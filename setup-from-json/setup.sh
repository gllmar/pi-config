#!/bin/bash
LOG_FILE="script_installation.log"
echo "Logging to $LOG_FILE..."

# Fetch script list JSON
fetch_script_list_json() {
    local url="$1"
    curl -sSL "$url" -o script_list.json
    if [[ $? -ne 0 || ! -s script_list.json ]]; then
        echo "Failed to fetch or empty script list JSON. Exiting."
        exit 1
    fi
    echo "Script list JSON fetched successfully."
}

# Display grouped scripts and allow selection
display_and_select_scripts() {
    local selected_scripts=()
    repositories=$(jq -r '.repositories | keys[]' script_list.json)

    echo "Available repositories:"
    select repo in $repositories; do
        if [[ -n "$repo" ]]; then
            base_url=$(jq -r ".repositories[\"$repo\"].base_url" script_list.json)
            setups=$(jq -r ".repositories[\"$repo\"].setup[]" script_list.json)

            echo "Available setups in $repo:"
            select setup in $setups; do
                if [[ -n "$setup" ]]; then
                    url="${base_url}${setup}/setup.sh"
                    selected_scripts+=("$url")
                    break
                else
                    echo "Invalid selection. Try again."
                fi
            done
        else
            echo "Invalid repository selection. Try again."
        fi
        break
    done

    echo "You selected the following scripts:"
    for script in "${selected_scripts[@]}"; do
        echo "$script"
    done

    echo "${selected_scripts[@]}"
}

# Execute selected scripts
execute_scripts() {
    local scripts=("$@")
    for script_url in "${scripts[@]}"; do
        local script_name=$(basename "$script_url")
        echo "Downloading and executing $script_name..."
        curl -sSL "$script_url" -o "$script_name"
        if [[ $? -eq 0 && -s "$script_name" ]]; then
            bash "$script_name" | tee -a "$LOG_FILE"
            echo "Executed $script_name successfully."
            rm "$script_name"
        else
            echo "Failed to download or empty script: $script_name. Skipping."
        fi
        echo
    done
}

# Main logic
read -p "Enter the URL of the script list JSON: " url
fetch_script_list_json "$url"

# Select and execute scripts
selected_scripts=$(display_and_select_scripts)
IFS=$'\n' read -r -d '' -a selected_scripts_array <<< "$selected_scripts"
execute_scripts "${selected_scripts_array[@]}"

echo "Script installation process completed. Check $LOG_FILE for details."
